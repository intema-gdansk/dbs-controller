import json
import sys
import time
import os

from PySide2.QtCore import Signal, QObject
from functools import wraps
from pymodbus.client.sync import ModbusSerialClient
from pymodbus.constants import Endian
from pymodbus.exceptions import ConnectionException
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.payload import BinaryPayloadDecoder
from queue import Queue
from serial.tools.list_ports import comports
from threading import Thread, Event
from tools.constants import ModbusStatus, DBSStatus, DBSCommand


class ModbusConnectorException(Exception):
    pass


def modbus_comm_wrapper(func):
    @wraps(func)
    def inner_func(*args, **kwargs):
        self = args[0]
        if self.dbs_status == ModbusStatus.OFF_LINE:
            print(f'Modbus client is offline')
            return False
        try:
            return func(*args, **kwargs)
        except Exception as e:
            self.change_modbus_status(ModbusStatus.OFF_LINE)
            self.change_dbs_status(DBSStatus.UNKNOWN)
            print(f'Modbus connector failed: {e}')
            return False
    return inner_func


class ModbusConnector(QObject):
    modbus_status_signal = Signal(int)
    dbs_status_signal = Signal(int)
    dbs_position_signal = Signal(tuple)

    def __init__(self, modbus_status_function=None, dbs_status_function=None, dbs_position_function=None,
                 debug_logger=None, config_path='./config.json'):
        super(ModbusConnector, self).__init__()
        self.debug_logger = debug_logger
        self.modbus_client = None
        self.devices_dict = {}

        # Load configuration file
        try:
            base_path = sys._MEIPASS if hasattr(sys, '_MEIPASS') else '.'
            with open(os.path.join(base_path, config_path), "r") as f:
                data = json.load(f)
            self.conf_reg = data['reg']
            self.conf_var = data['var']
            self.conf_com = data['com']
        except Exception as e:
            raise Exception(f'Failed to load Modbus config: {e}')

        # Modbus config
        self.wo, self.bo = self.conf_var['wo'], self.conf_var['bo']
        self.modbus_status = ModbusStatus.UNKNOWN
        self.unit = self.conf_com['unit']

        # DBS state
        self.dbs_status = DBSStatus.UNKNOWN
        self.dbs_position = None
        self.dbs_serial = 0
        self.last_cmd = DBSCommand.RESET

        self.req_reset_payload = None
        self.req_tray_payload = None

        self.modbus_status_function = modbus_status_function
        if modbus_status_function:
            self.modbus_status_signal.connect(modbus_status_function)

        self.dbs_status_function = dbs_status_function
        if dbs_status_function:
            self.dbs_status_signal.connect(dbs_status_function)

        self.dbs_position_function = dbs_position_function
        if dbs_position_function:
            self.dbs_position_signal.connect(dbs_position_function)

        # control registers
        self.adr_w_instruction = self.conf_reg['cmd']
        self.adr_w_instruction_arg = self.conf_reg['cmd_arg']
        self.adr_r_spot_pos = self.conf_reg['pos']
        self.adr_r_dbs_state = self.conf_reg['state']
        self.adr_r_dbs_serial = self.conf_reg['serial']
        self.adr_w_sensor_enable = self.conf_reg['sensor']
        self.adr_w_cut_test_proc = self.conf_reg['cut_test']

        self.stop_event = Event()
        self.reset_event = Event()
        self.reset_event.clear()
        self.stop_event.clear()
        self.cmd_payload_queue = Queue()
        self.comm_thread = Thread(target=self.communication_loop_thread, args=(self.stop_event,))

    def _log_debug_msg(self, log_message: str):
        """
        Uses logger to save debug message
        """
        if self.debug_logger is not None:
            self.debug_logger.info(f'MODBUS: {log_message}')

    def scan_for_devices(self):
        """
        Scan for serial devices and update devices dict
        """
        devices = comports()
        for d_idx in range(len(devices)-1, -1, -1):
            if self.modbus_client and self.modbus_client.port == devices[d_idx].device:
                continue
            try:
                mc = self.create_modbus_client(devices[d_idx].device)
                if not mc.connect():
                    devices.pop(d_idx)
                    continue
                serial = mc.read_holding_registers(self.adr_r_dbs_serial).registers[0]
                mc.close()
            except Exception:
                devices.pop(d_idx)

        self.devices_dict = {dev.name: dev for dev in devices}

    def create_modbus_client(self, port_name: str):
        """
        Return ModbusSerialClient connected to DBS at given port

        :param port_name: Serial port name
        """
        return ModbusSerialClient(port=port_name, **self.conf_com)

    def connect_to_modbus(self, device_name: str):
        """
        Connect to modbus

        :param device_name: Device in devices_dict name
        """
        try:
            device = self.devices_dict[device_name]
            if self.modbus_client and self.is_alive() and self.modbus_client.port == device.device:
                return
            self.modbus_client = self.create_modbus_client(device.device)
            if self.modbus_client.connect():
                # Change status and initialize command Queue
                self.change_modbus_status(ModbusStatus.ON_LINE)
                self.cmd_payload_queue = Queue()

                # make sure current comm thread is dead
                if self.comm_thread and self.comm_thread.is_alive():
                    self.stop_event.set()
                    self.comm_thread.join()
                self.stop_event.clear()

                # New comm thread
                self.comm_thread = Thread(target=self.communication_loop_thread, args=(self.stop_event,))
                self.comm_thread.start()
                self.read_device_serial()
            else:
                self.change_modbus_status(ModbusStatus.OFF_LINE)
                self.change_dbs_status(DBSStatus.UNKNOWN)
        except (ConnectionException, OSError, AttributeError) as e:
            self.modbus_client = None
            self.change_modbus_status(ModbusStatus.UNKNOWN)
            self.change_dbs_status(DBSStatus.UNKNOWN)

    def communication_loop_thread(self, stop_event):
        """
        Main communication loop. Reads commands from Queue and executes them if DBS state is Idle.
        Updates DBS state in loop.

        :param stop_event: Stop event
        """
        while not stop_event.is_set():
            if self.modbus_status == ModbusStatus.ON_LINE:
                dbs_status = self.dbs_status
                # Clear queue after reset
                if self.req_reset_payload is not None:
                    self._log_debug_msg('CLT: req_reset_payload: True')
                    while not self.cmd_payload_queue.empty():
                        self.cmd_payload_queue.get()
                    # Wait for movement to end
                    if not (self.last_cmd in [DBSCommand.MOVE, DBSCommand.TRAY] and dbs_status == DBSStatus.BUSY):
                        self.write_cmd_payload(self.req_reset_payload)
                        if self.req_tray_payload:
                            self._log_debug_msg('CLT: req_tray_payload: True')
                            self.write_cmd_payload(self.req_tray_payload)
                        self.req_reset_payload = self.req_tray_payload = None
                    self._log_debug_msg('CLT: Reset proc finished')
                # Read and execute command from queue
                if dbs_status == DBSStatus.IDLE and not self.cmd_payload_queue.empty():
                    self._log_debug_msg('CLT: Reading queue...')
                    payload = self.cmd_payload_queue.get()
                    self._log_debug_msg(f'CLT: Got payload from the queue: {payload}')
                    ret = self.write_cmd_payload(payload)
                    self._log_debug_msg(f'CLT: Cmd executed')
                    # time.sleep(0.4)
                # Update device status
                self.read_device_state()
                self.read_current_tray_hole()
            else:
                time.sleep(0.1)

    def write_cmd_move(self, column=0, row=0, side='L') -> bool:
        """
        Send MOVE instruction to DBS

        :param column: plate column - 0-7
        :param row: plate row - 0-11
        :param side: which side of the tray we work on - ['L', 'R']
        :return: If communication succeeded
        """
        if row > 11 or column > 7:
            raise Exception('Invalid move command')
        payload = self.create_cmd_move_payload(column, row, side)
        return self.write_cmd_payload(payload)

    def create_cmd_move_payload(self, column=0, row=0, side='L') -> list:
        """
        Creates a payload for MOVE instruction
        """
        builder = self.get_payload_builder()
        builder.add_16bit_int(256*ord(DBSCommand.MOVE[0]) + ord(DBSCommand.MOVE[1]))
        builder.add_16bit_int(ord(side))
        builder.add_16bit_int(65+column)
        builder.add_16bit_int(row+1)
        payload = builder.build()
        return payload

    def dequeue_cmd_move(self, column=0, row=0, side='L'):
        """
        Puts MOVE command to cmd queue

        :param column: plate column - 0-7
        :param row: plate row - 0-11
        :param side: which side of the tray we work on - ['L', 'R']
        """
        self.cmd_payload_queue.put(self.create_cmd_move_payload(column, row, side))
        self._log_debug_msg('Queue: move_payload')

    def dequeue_cmd_tray(self, posOI='O', posLMR='M') -> bool:
        """
        Send TRAY instruction to DBS

        :param posOI: moves the tray to outermost position ('O') or starting position ('I') - ['O', 'I']
        :param posLMR: moves the tray to the far left, middle, or far right position - ['L', 'M', 'R']
        :return: If communication succeeded
        """
        self.req_tray_payload = self.create_cmd_tray_payload(posOI, posLMR)
        self.req_reset_payload = self.create_cmd_reset_payload(hard_reset=False)
        self._log_debug_msg('Tray cmd requested')
        return True

    def create_cmd_tray_payload(self, posOI='O', posLMR='M') -> list:
        """
        Creates a payload for TRAY instruction
        """
        builder = self.get_payload_builder()
        builder.add_16bit_int(256*ord(DBSCommand.TRAY[0]) + ord(DBSCommand.TRAY[1]))
        builder.add_16bit_int(ord(posOI))
        builder.add_16bit_int(ord(posLMR))
        builder.add_16bit_int(0)
        payload = builder.build()
        return payload

    def write_cmd_tray(self, posOI='O', posLMR='M'):
        """
        Puts TRAY command to cmd queue

        :param posOI: moves the tray to outermost position ('O') or starting position ('I') - ['O', 'I']
        :param posLMR: moves the tray to the far left, middle, or far right position - ['L', 'M', 'R']
        """
        self.write_cmd_payload(self.create_cmd_tray_payload(posOI, posLMR))

    def write_cmd_cut(self, no_cuts=1) -> bool:
        """
        Send CUT instruction do DBS

        :param no_cuts: Number of cuts - any int16 number
        :return: If communication succeeded
        """
        payload = self.create_cmd_cut_payload(no_cuts)
        return self.write_cmd_payload(payload)

    def create_cmd_cut_payload(self, no_cuts=1) -> list:
        """
        Creates a payload for CUT instruction
        """
        no_cuts = 1  # TODO? values greater than that can break the machine - DBS always makes 2 cuts anyway
        builder = self.get_payload_builder()
        builder.add_16bit_int(256*ord(DBSCommand.CUT[0]) + ord(DBSCommand.CUT[1]))
        builder.add_16bit_int(no_cuts)
        builder.add_16bit_int(0)
        builder.add_16bit_int(0)
        payload = builder.build()
        return payload

    def dequeue_cmd_cut(self, no_cuts=1):
        """
        Puts TRAY command to cmd queue

        :param no_cuts: Number of cuts - any int16 number
        """
        self.cmd_payload_queue.put(self.create_cmd_cut_payload(no_cuts))
        self._log_debug_msg('Queue: cut_payload')

    def write_cmd_reset(self, hard_reset=False, immediately=False) -> bool:
        """
        Send RESET instruction do DBS. Soft reset command will be executed in processing loop
        unless immediately flag is set to True

        :param hard_reset: If hard reset should be executed
        :param immediately: If soft reset should be executed immediately
        :return: If communication succeeded
        """
        payload = self.create_cmd_reset_payload(hard_reset=hard_reset)
        self._log_debug_msg(f'Reset:  hard_reset: {hard_reset};  immediately: {immediately}')
        if immediately:
            return self.write_cmd_payload(payload)
        self.req_reset_payload = payload
        return True

    def create_cmd_reset_payload(self, hard_reset=1) -> list:
        """
        Creates a payload for RESET instruction
        """
        builder = self.get_payload_builder()
        builder.add_16bit_int(256 * ord(DBSCommand.RESET[0]) + ord(DBSCommand.RESET[1]))
        builder.add_16bit_int(int(hard_reset) + 1)
        builder.add_16bit_int(0)
        builder.add_16bit_int(0)
        payload = builder.build()
        return payload

    @modbus_comm_wrapper
    def write_cmd_payload(self, payload: list):
        """
        Send payload data to cmd registers

        :param payload: Payload data
        :return: If communication succeeded
        """
        self.last_cmd = chr(payload[0][0]) + chr(payload[0][1])
        ret = self.modbus_client.write_registers(self.adr_w_instruction, payload, skip_encode=True, unit=self.unit)
        success = not ret.isError()
        self._log_debug_msg(f'Updated registers;  msg: {self.last_cmd};  success: {success};  payload: {payload}')
        return success

    @modbus_comm_wrapper
    def read_current_tray_hole(self) -> bool:
        """
        Read and update position of currently selected hole in the tray

        :return: If communication succeeded
        """
        result = self.modbus_client.read_holding_registers(self.adr_r_spot_pos, 3, unit=self.unit)
        decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=self.bo, wordorder=self.wo)
        side = chr(decoder.decode_16bit_int())
        col = decoder.decode_16bit_int() - 65
        row = decoder.decode_16bit_int() - 1
        if 7 >= col >= 0 and 11 >= row >= 0:
            self.change_dbs_position((col, row, side))
        else:
            self.change_dbs_position((-1, -1, ''))
        return True

    @modbus_comm_wrapper
    def read_device_state(self) -> bool:
        """
        Read and update DBS state

        :return: If communication succeeded
        """
        result = self.modbus_client.read_holding_registers(self.adr_r_dbs_state, 1,  unit=self.unit)
        decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=self.bo, wordorder=self.wo)
        decoded = decoder.decode_16bit_int()
        status_code = chr((decoded >> 8) & 255) + chr(decoded & 255)
        if status_code == 'BY':
            status = DBSStatus.BUSY
        elif status_code == 'ID':
            status = DBSStatus.IDLE
        else:  # status_code == 'ER':
            status = DBSStatus.ERROR
        self.change_dbs_status(status)
        return True

    @modbus_comm_wrapper
    def read_device_serial(self) -> bool:
        """
        Read and update DBS device serial number

        :return: If communication succeeded
        """
        result = self.modbus_client.read_holding_registers(self.adr_r_dbs_serial, 1,  unit=self.unit)
        self.dbs_serial = result.registers[0]
        return True

    def change_modbus_status(self, status: ModbusStatus):
        """
        Modbus status changer.

        :param status: goal status
        """
        if self.modbus_status != status:
            if status == ModbusStatus.OFF_LINE or status == ModbusStatus.OFF_LINE:
                self.stop_event.set()
                # self.modbus_client

            self._log_debug_msg(f'modbus_status: {self.modbus_status.name} -> {status.name}')
            self.modbus_status = status
            self.modbus_status_signal.emit(status.value)

    def change_dbs_position(self, position: tuple):
        """
        DBS position changer
        :param position: current position
        """
        if self.dbs_position != position:
            self.dbs_position = position
            self.dbs_position_signal.emit(position)

    def change_dbs_status(self, status: DBSStatus):
        """
        DBS status changer.
        """
        if status != self.dbs_status:
            self.dbs_status = status
            self.dbs_status_signal.emit(status.value)
            self._log_debug_msg(f'dbs_status: {self.dbs_status.name}')

    def get_payload_builder(self) -> BinaryPayloadBuilder:
        """
        Create modbus BinaryPayloadBuilder
        """
        builder = BinaryPayloadBuilder(wordorder=self.wo, byteorder=self.bo)
        return builder

    def stop_procedure(self):
        """
        Stops the communication loop
        """
        self._log_debug_msg('Stop procedure')
        self.stop_event.set()
        if self.modbus_client is not None:
            self.change_modbus_status(ModbusStatus.OFF_LINE)
            self.change_dbs_status(DBSStatus.UNKNOWN)
            # TODO? 'NoneType' object has no attribute 'hEvent' - Windows only
            try:
                self.modbus_client.close()
            except Exception as e:
                print(e)
            self.modbus_client = None

    def is_alive(self):
        return self.modbus_status == ModbusStatus.ON_LINE and not self.stop_event.is_set()

    def __del__(self):
        self.stop_procedure()

    def enable_spot_sensor(self, enable=True):
        """
        Enables a test for the presence of a sample in the selected spot after cut procedure.
        """
        builder = self.get_payload_builder()
        builder.add_16bit_int(self.conf_var['spot_sensor'] if enable else 0)
        payload = builder.build()
        ret = self.modbus_client.write_registers(self.adr_w_sensor_enable, payload, skip_encode=True, unit=self.unit)

    def enable_test_cut_proc(self, enable_test):
        """
        Enables the test mode of the cut procedure.
        """
        builder = self.get_payload_builder()
        builder.add_16bit_int(self.conf_var['test_cut'] if enable_test else 0)
        payload = builder.build()
        ret = self.modbus_client.write_registers(self.adr_w_cut_test_proc, payload, skip_encode=True, unit=self.unit)
        print(ret.isError())


if __name__ == '__main__':
    m = ModbusConnector(config_path='../config.json')
    m.scan_for_devices()
    m.connect_to_modbus(list(m.devices_dict.keys())[0])
    m.enable_test_cut_proc(False)
    # print(m.read_current_tray_hole(
    m.stop_event.set()
