from enum import Enum

column_names = 'ABCDEFGH'


def get_color(status: int):
    status_color = {1: 'gre',
                    10: 'red',
                    20: 'yel',
                    30: 'gry',
                    100: 'red'}
    return status_color[status]


class DBSCutProcStep(Enum):
    START = 0
    MOVE = 1
    NAME = 2
    MOVE_WAIT = 3
    CUT = 4
    CUT_WAIT = 5
    END = 100


class ModbusStatus(Enum):
    ON_LINE = 1
    OFF_LINE = 10
    UNKNOWN = 30
    CLOSED = 10


class DBSStatus(Enum):
    IDLE = 1
    BUSY = 20
    ERROR = 10
    UNKNOWN = 30


class DBSCommand:
    MOVE = 'MV'
    RESET = 'RS'
    CUT = 'CT'
    TRAY = 'TR'


class SpotState(Enum):
    """
    Hole state enum:
    EMPTY - There is nothing in hole (reset)
    CUTTER_GOAL - DBS tries to go the selected hole
    UNDER_CUTTER - DBS is under the selected goal - allowing to cut
    PROCESS - DBS process current slot
    WITH_PUNCH - Something was cut into the hole
    """
    EMPTY = 0
    CUTTER_GOAL = 1
    UNDER_CUTTER = 2
    WITH_PUNCH = 3
    PROCESS = 4
    BLOCK = 9


class DBSMode(Enum):
    LEFT = 0
    BOTH = 1


class DBSTraySide(Enum):
    L = 0
    R = 1

