import datetime
from openpyxl import Workbook
from openpyxl.styles import Alignment, Font, Border, Side, Color, Fill, PatternFill


def save_plate_spreadsheet(path: str, plate: dict, plate_name: str, operator_name: str):
    wb = Workbook()

    # Format objects
    f_table_bold = Font(b=True, name='Calibri', size=11)
    f_table_normal = Font(b=False, name='Calibri', size=11)
    f_header_bold = Font(b=True, name='Calibri', size=12)
    f_header_normal = Font(b=False, name='Calibri', size=12)
    alignment = Alignment(horizontal="center", vertical="center")
    s_bold = Side(border_style="thick", color="000000")
    s_thin = Side(border_style="thin", color="000000")
    b_bold = Border(top=s_bold, left=s_bold, right=s_bold, bottom=s_bold)
    b_table = Border(top=None, left=s_thin, right=s_thin, bottom=None)
    b_thin = Border(top=s_thin, left=s_thin, right=s_thin, bottom=s_thin)
    b_table_mid = Border(top=None, left=s_thin, right=s_thin, bottom=s_thin)
    c_gray = Color('d3d3d3')
    c_yell = Color('f0e68c')
    f_gray = PatternFill(patternType='solid', fgColor=c_gray)
    f_yell = PatternFill(patternType='solid', fgColor=c_yell)

    # ==== ==== ==== Samples list ==== ==== ====
    ws1 = wb.create_sheet('Samples list', 0)

    ws1.row_dimensions[1].height = 37
    ws1.row_dimensions[2].height = 37
    for idx in range(1, 13):
        ws1.column_dimensions[chr(64+idx)].width = 15
        ws1.row_dimensions[idx+1].font = f_table_normal

    # Alignment
    for r in range(1, 110):
        for c in range(1, 15):
            ws1.cell(row=r, column=c).alignment = alignment

    # Fonts format
    ws1.row_dimensions[1].font = f_header_bold
    ws1.row_dimensions[2].font = f_header_bold
    ws1.cell(row=1, column=4).font = f_header_normal
    ws1.cell(row=2, column=5).font = f_header_normal

    # Merge cells
    ws1.merge_cells('A1:C1')
    ws1.merge_cells('D1:E1')
    ws1['A1'] = 'Plate name:'
    ws1['D1'] = f'{plate_name}'

    plate = {k: v for k, v in plate.items() if v.is_processing_finished()}

    # Fill cells
    for cell_n, cell_v in enumerate(['No', 'Sample code', 'Position', 'Samples count:', f'{len(plate)}']):
        ws1.cell(row=2, column=cell_n+1).value = cell_v

    spots = sorted(plate.values(), key=lambda x: x.id[0]*100 + x.id[1])
    for no, sp in enumerate(spots):
        ws1.cell(row=3 + no, column=1).value = no + 1
        ws1.cell(row=3 + no, column=2).value = sp.name
        ws1.cell(row=3 + no, column=3).value = sp.get_pos_name()
        # Format
        ws1.cell(row=3 + no, column=1).border = b_table
        ws1.cell(row=3 + no, column=1).fill = f_gray
        ws1.cell(row=3 + no, column=2).border = b_table_mid
        ws1.cell(row=3 + no, column=2).fill = f_yell
        ws1.cell(row=3 + no, column=2).font = f_table_bold
        ws1.cell(row=3 + no, column=3).border = b_table

    # Borders
    for r in range(1, 3):
        for c in range(1, 6):
            ws1.cell(row=r, column=c).border = b_bold
    # ==== ==== ==== Plate array ==== ==== ====
    ws2 = wb.create_sheet('Plate', 1)
    for idx in range(1, 14):
        ws2.column_dimensions[chr(64+idx)].width = 12
        ws2.row_dimensions[idx].height = 27
    ws2.row_dimensions[1].height = 38
    ws2.row_dimensions[2].height = 38

    # Alignment
    for r in range(1, 15):
        for c in range(1, 15):
            ws2.cell(row=r, column=c).alignment = alignment
    # Fonts
    ws2.row_dimensions[1].font = f_header_normal
    ws2.row_dimensions[2].font = f_header_normal
    ws2.column_dimensions['A'].font = f_table_bold
    ws2.row_dimensions[4].font = f_table_bold

    # Header borders
    for c in range(1, 14):
        ws2.cell(row=1, column=c).border = b_bold
    for c in range(1, 9):
        ws2.cell(row=2, column=c).border = b_bold

    # Header
    ws2.merge_cells('A1:C1')
    ws2.merge_cells('D1:E1')
    ws2.merge_cells('G1:H1')
    ws2.merge_cells('I1:J1')
    ws2.merge_cells('K1:M1')
    ws2.merge_cells('A2:C2')
    ws2.merge_cells('D2:H2')
    ws2['A1'] = 'Plate name:'
    ws2['D1'] = f'{plate_name}'
    date = datetime.datetime.now()
    ws2['F1'] = 'Date'
    ws2['G1'] = date.strftime("%d.%m.%Y")
    ws2['I1'] = 'Operator:'
    ws2['K1'] = f'{operator_name}'
    ws2['A2'] = 'Comments:'

    # Table borders
    for r in range(5, 13):
        for c in range(2, 14):
            ws2.cell(row=r, column=c).border = b_thin

    for r in range(5, 13):
        ws2.cell(row=r, column=1).fill = f_gray
        ws2.cell(row=r, column=1).border = b_bold
        ws2.cell(row=r, column=1).font = f_table_bold
    for c in range(2, 14):
        ws2.cell(row=4, column=c).border = b_bold
        ws2.cell(row=4, column=c).fill = f_gray
        ws2.cell(row=4, column=c).font = f_table_bold

    # Table
    for idx in range(12):
        ws2.cell(row=4, column=2 + idx).value = f'{idx + 1}'
    for idx in range(8):
        ws2.cell(row=5+idx, column=1).value = chr(65+idx)

    for spot in [v for v in plate.values()]:
        ws2.cell(row=spot.id[0]+5, column=spot.id[1]+2).value = spot.name
    if len(wb.sheetnames) == 3:
        wb.remove_sheet(wb.get_sheet_by_name('Sheet'))
    wb.save(path)
