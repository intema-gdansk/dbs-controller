import copy
import logging
import sys
from logging import handlers, LoggerAdapter, Formatter


class CustomLoggerAdapter(LoggerAdapter):
    """
    Custom logger adapter for multiple subsystems as parameter
    """
    def __init__(self, basic_logger, extra):
        """
        Initialization of adapter based on logger and extra parameters
        :param basic_logger: logger
        :param extra: parameters
        """
        super(CustomLoggerAdapter, self).__init__(basic_logger, extra)
        self.env = extra

    def process(self, msg, kwargs):
        """
        Overrides normal processing of logger
        :param msg: massage
        :param kwargs: additional arguments (eg subsystem)
        :return: msg, dict of updated kwargs
        """
        msg, kwargs = super(CustomLoggerAdapter, self).process(msg, kwargs)
        result = copy.deepcopy(kwargs)
        default_kwargs_key = ['exc_info', 'stack_info', 'extra']
        custom_key = [k for k in result.keys() if k not in default_kwargs_key]
        result['extra'].update({k: result.pop(k) for k in custom_key})
        if 'subsystem' not in custom_key:
            result['extra'].update({'subsystem': 'Default'})
        return msg, result


def setup_custom_logger(name):
    """
    Setup of custom logger
    :param name: name of logger
    :return: logger ready to use
    """
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG)
    log_format = Formatter('%(asctime)s.%(msecs)03d  | %(levelname)s | %(name)s / %(subsystem)s :: %(message)s',
                           datefmt='%Y-%m-%d, %H:%M:%S')
    try:
        from colorlog import ColoredFormatter
        clog_format = ColoredFormatter(
            "%(asctime)s.%(msecs)03d | %(log_color)s%(levelname)-5s%(reset)s | %(yellow)s%(name)-5.5s%(reset)s"
            "/%(purple)s%(subsystem)18s%(reset)s :: %(blue)s%(message)s",
            datefmt='%Y-%m-%d, %H:%M:%S',
            reset=True,
            log_colors={
                'DEBUG': 'cyan',
                'INFO': 'blue',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'red,bg_white',
            },
            secondary_log_colors={},
            style='%')
    except ImportError:
        clog_format = log_format
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setFormatter(clog_format)
    log.addHandler(stream_handler)
    file_handler = handlers.RotatingFileHandler('dbs.log', maxBytes=(1048576 * 5), backupCount=10)
    file_handler.setFormatter(log_format)
    log.addHandler(file_handler)
    log = CustomLoggerAdapter(log, extra={})
    return log


logger = setup_custom_logger('DBS')
