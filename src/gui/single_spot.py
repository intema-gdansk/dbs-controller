from PySide2 import QtCore
from PySide2.QtCore import Signal, QObject, Slot
from PySide2.QtGui import QFont, QRadialGradient, QColor
from PySide2.QtWidgets import QGraphicsItem, QMenu, QGraphicsItemGroup
from tools.constants import column_names, SpotState


class QSpotsItemGroup(QGraphicsItemGroup):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.setHandlesChildEvents(True)


class Emitter(QObject):
    spot_click_signal = Signal(tuple)
    log_trigger_signal = Signal()


class SpotGraphicItem(QGraphicsItem):
    """
    Single QGraphicsItem which describes one hole in multi-hole tray.
    """
    size = 100

    def __init__(self, row, column, tray, horizontal=True):
        """
        Constructor
        :param row: row of the hole
        :param column: column of the hole
        :param tray: code of tray (L or R)
        :param horizontal: if the tray should be drawn vertically or horizontally
        """
        super(SpotGraphicItem, self).__init__()
        self.id = (column, row, tray)
        self.horizontal = horizontal

        self.emitter = Emitter()
        self.state = SpotState.EMPTY
        self._update_rectF()
        self.name = ""
        self.description = ""
        self.update()

    def _update_rectF(self):
        """
        Updates QGraphicsItem rectF
        """
        self.half = int(self.size / 2)
        self.y = self.half + self.id[0] * int(1.1 * self.size)
        self.x = self.half + self.id[1] * int(1.1 * self.size)
        if not self.horizontal:
            self.x, self.y = self.y, self.x
        self.rectF = QtCore.QRectF(-self.half + self.x, -self.half + self.y, self.size, self.size)

    def boundingRect(self):
        """
        Bounding rect method (position on canvas)
        """
        return self.rectF

    def paint(self, painter=None, style=None, widget=None):
        """
        Paints circle with gradient in three colors depending on state, with description (text) of hole number.
        Inherits form QGraphicItem
        """
        painter.setFont(QFont('Arial', 14))
        gradient = QRadialGradient(self.x, self.y, self.half)
        if self.state == SpotState.WITH_PUNCH:
            gradient.setColorAt(0, QColor.fromRgbF(1, 0, 0, 1))
            painter.setPen(QtCore.Qt.white)
        elif self.state == SpotState.CUTTER_GOAL:
            gradient.setColorAt(0, QColor.fromRgbF(1, 1, 0, 1))
            painter.setPen(QtCore.Qt.black)
        elif self.state == SpotState.UNDER_CUTTER:
            gradient.setColorAt(0, QColor.fromRgbF(0, 0, 1, 1))
            painter.setPen(QtCore.Qt.white)
        elif self.state == SpotState.PROCESS:
            gradient.setColorAt(0, QColor.fromRgbF(0.7, 1, 0, 1))
            painter.setPen(QtCore.Qt.black)
        elif self.state == SpotState.EMPTY:
            gradient.setColorAt(0, QColor.fromRgbF(1, 1, 1, 1))
            painter.setPen(QtCore.Qt.black)
        elif self.state == SpotState.BLOCK:
            gradient.setColorAt(0, QColor.fromRgbF(0.2, 0.2, 0.3, 1))
            painter.setPen(QtCore.Qt.white)
        gradient.setColorAt(0.8, QColor.fromRgbF(0.2, 0.2, 0.3, 0.15))
        painter.fillRect(self.rectF, gradient)
        painter.drawText(self.rectF, QtCore.Qt.AlignCenter, f'{column_names[self.id[0]]}{self.id[1] + 1}')
        if self.horizontal and self.state in [SpotState.WITH_PUNCH, SpotState.UNDER_CUTTER]:
            painter.setPen(QtCore.Qt.black)
            painter.setFont(QFont('Arial', 9))
            painter.drawText(self.rectF, QtCore.Qt.AlignBottom, f'{self.name}')

    @Slot()
    def reset(self):
        """
        Reset state
        """
        self.state = SpotState.EMPTY
        self.update()

    def spot_number(self):
        """
        Return spot number
        """
        return self.id[0] * 8 + self.id[1]

    def get_pos_name(self):
        """
        Return position nanem
        """
        return f'{chr(65 + self.id[0])}{self.id[1] + 1}'

    def confirm_punch(self):
        """
        Confirms punch
        """
        if self.state == SpotState.UNDER_CUTTER or self.state == self.state.PROCESS:
            self.state = SpotState.WITH_PUNCH
            self.update()
            self.emitter.log_trigger_signal.emit()

    def confirm_process(self):
        """
        Confirms process
        """
        self.state = SpotState.PROCESS
        self.update()

    def confirm_position(self):
        """
        Confirms position of dbs
        """
        self.state = SpotState.UNDER_CUTTER
        self.update()

    def confirm_block(self):
        """
        Confirms position of dbs
        """
        if self.is_empty():
            self.state = SpotState.BLOCK
        self.update()

    def confirm_goal(self):
        """
        Confirms goal of dbs
        """
        if self.state == SpotState.EMPTY:
            self.state = SpotState.CUTTER_GOAL
        self.update()

    @Slot(SpotState)
    def update_state_slot(self, spot_state: SpotState):
        """
        Updates spot's state.
        """
        if spot_state == SpotState.PROCESS:
            self.confirm_process()
        elif spot_state == SpotState.UNDER_CUTTER:
            self.confirm_position()
        elif spot_state == SpotState.WITH_PUNCH:
            self.confirm_punch()
        elif spot_state == SpotState.CUTTER_GOAL:
            self.confirm_goal()

    def is_empty(self):
        return self.state == SpotState.EMPTY

    def is_blocked(self):
        return self.state == SpotState.BLOCK

    def is_goal(self):
        return self.state == SpotState.CUTTER_GOAL

    def is_processing_finished(self):
        return self.state == SpotState.WITH_PUNCH

    def contextMenuEvent(self, event):
        """
        Spot context menu
        """
        menu = QMenu()
        clean_action = menu.addAction("Clean")
        block_action = menu.addAction("Block")
        delete_action = menu.addAction("Add Description")
        delete_action.setEnabled(False)
        if self.is_empty() or self.is_blocked() or not self.is_processing_finished():
            clean_action.setEnabled(False)
        if not self.is_empty() and not self.is_blocked():
            block_action.setEnabled(False)

        if self.is_blocked():
            block_action.setText('Unblock')

        action = menu.exec_(event.screenPos())
        if action == clean_action or (self.is_blocked() and action == block_action):
            self.reset()
            self.emitter.log_trigger_signal.emit()
        elif action == block_action:
            self.confirm_block()
            self.emitter.log_trigger_signal.emit()

    def mousePressEvent(self, event):
        """
        On left mouse press event
        :param event: type of Qt Event
        """
        if event.button() == QtCore.Qt.LeftButton:
            self.confirm_goal()
            self.emitter.spot_click_signal.emit(self)
        self.update()

    def sceneEvent(self, event: QtCore.QEvent):
        if event.type() == QtCore.QEvent.GraphicsSceneContextMenu:
            self.contextMenuEvent(event)
        if event.type() == QtCore.QEvent.GraphicsSceneMousePress:
            self.mousePressEvent(event)
        event.accept()
        return True
