import logging
import sys
import time
from logging.handlers import RotatingFileHandler
from threading import Event, Thread
from PySide2.QtCore import Slot, QEvent, Signal
from PySide2.QtGui import QIcon, QPixmap, Qt
from PySide2.QtWidgets import QMainWindow, QApplication, QFileDialog, QGraphicsScene, QActionGroup, QAction, QMenu, \
    QMessageBox, QGraphicsItemGroup

from gui.single_spot import SpotGraphicItem, QSpotsItemGroup
from gui.dbs_legend_dialog import DBSLegendDialog
from gui.uis.working_gui import Ui_DryBloodWindow
from tools.constants import DBSMode, DBSCutProcStep, DBSStatus, DBSCommand, get_color, DBSTraySide, SpotState
from tools.modbus_client import ModbusConnector

__author__ = '(michal & jasiu) @ Intema Sp. z o. o.'
__version__ = 'Dry Blood System Controller v 1.1.0'
__DEBUG__ = True

from tools.xlsx_writer import save_plate_spreadsheet


class DryBloodSystem(QMainWindow):
    """
    Bry Blood System Cutter main class
    """
    dbs_proc_error_dialog_signal = Signal(str)
    dbs_proc_name_edit_enable_signal = Signal(bool)
    dbs_proc_update_spot_signal = Signal(SpotGraphicItem, SpotState)
    dbs_proc_reset_spot_signal = Signal(SpotGraphicItem)
    dbs_proc_spot_click_signal = Signal(SpotGraphicItem)
    dbs_proc_reset_tray_buttons_enable_signal = Signal(bool)
    dbs_proc_restore_state_action_enable_signal = Signal(bool)
    dbs_proc_pass_enable_signal = Signal(bool)

    def __init__(self):
        """
        Constructor
        Loading and setting up ui
        """
        super(DryBloodSystem, self).__init__()
        # Loads ui from file
        self.ui = Ui_DryBloodWindow()
        self.ui.setupUi(self)
        # Setting name of widow
        self.setWindowTitle(__version__)
        self.setWindowIcon(QIcon(':/icons/icons/Red_blood_cell.png'))

        # Main loop thread setup
        self.last_state_log = None
        self.dbs_service_mode = False
        self.dbs_sample_presence_test_mode = False
        self.dbs_proc_error_ignore = False
        self.dbs_sample_name = ''
        self.dbs_proc_thread = Thread(target=self.dbs_proc_loop_thread)

        self.dbs_proc_stop_event = Event()
        self.dbs_proc_pause_event = Event()
        self.dbs_proc_skip_event = Event()
        self.dbs_proc_dialog_show_event = Event()

        self.dbs_proc_error_dialog_signal.connect(self.dbs_proc_ignore_error_dialog)
        self.dbs_proc_name_edit_enable_signal.connect(self.dbs_proc_enable_name_edit)
        self.dbs_proc_update_spot_signal.connect(lambda spot, spot_state: spot.update_state_slot(spot_state))
        self.dbs_proc_reset_spot_signal.connect(lambda spot: spot.reset())
        self.dbs_proc_spot_click_signal.connect(lambda spot_id: self.spot_click_slot(spot_id))
        self.dbs_proc_reset_tray_buttons_enable_signal.connect(lambda x: self.dbs_proc_enable_reset_tray_buttons(x))
        self.dbs_proc_pass_enable_signal.connect(lambda x: self.dbs_proc_enable_pass(x))
        self.dbs_proc_restore_state_action_enable_signal.connect(self.dbs_proc_enable_restore_state_action)

        # Set up logger
        self.logger = self._setup_logger('log', './.dbs_app.log')
        self.debug_logger = self._setup_logger('debug', './.dbs_app_debug.log', log_time=True) if __DEBUG__ else None

        # Setting pixmaps
        self.tool_width = self.ui.tool_widget.size().width()
        self.left_spot_item_group = QSpotsItemGroup()
        self.right_spot_item_group = QSpotsItemGroup()
        self.pixmaps = {'blu': QPixmap(':/icons/icons/Blue_Light_Icon.svg').scaled(25, 25, Qt.KeepAspectRatio),
                        'gry': QPixmap(':/icons/icons/Gray_Light_Icon.svg').scaled(25, 25, Qt.KeepAspectRatio),
                        'red': QPixmap(':/icons/icons/Red_Light_Icon.svg').scaled(25, 25, Qt.KeepAspectRatio),
                        'yel': QPixmap(':/icons/icons/Yellow_Light_Icon.svg').scaled(25, 25, Qt.KeepAspectRatio),
                        'gre': QPixmap(':/icons/icons/Green_Light_Icon.svg').scaled(25, 25, Qt.KeepAspectRatio)}

        self._dialog_options = QFileDialog.Options()
        self._dialog_options |= QFileDialog.DontUseNativeDialog
        self.legend_dialog = DBSLegendDialog()

        # Modbus bridge
        self.ui.dbs_status.setPixmap(self.pixmaps['gry'])
        self.ui.modbus_status.setPixmap(self.pixmaps['gry'])
        self.modbus_bridge = None
        try:
            self.modbus_bridge = ModbusConnector(dbs_status_function=self.update_dbs_status,
                                                 modbus_status_function=self.update_modbus_status,
                                                 dbs_position_function=self.update_dbs_position,
                                                 debug_logger=self.debug_logger)
        except Exception as e:
            print(e)
            self._log_debug_msg(str(e))
            self.close()
        self.modbus_devices_menu = QMenu("menu")
        self.modbus_devices_ag = QActionGroup(self.modbus_devices_menu, exclusive=True)
        self.modbus_devices_ag.triggered.connect(self.connect_device)
        self.ui.actionModbus_Connect.setMenu(self.modbus_devices_menu)

        # Currents
        self.mode = DBSMode.LEFT
        self.current_tray = DBSTraySide.L
        self.target_position = None  # column[0-7];  row[0-11];

        # Trays and graphic scene
        self.ui.left_graphicsView.setStyleSheet(f"background-color: {self.palette().background().color().name()};")
        self.ui.right_graphicsView.setStyleSheet(f"background-color: {self.palette().background().color().name()};")
        self._init_scene()

        # Actions
        self.ui.actionModbus_Scan.triggered.connect(self.scan_for_devices)
        self.ui.actionModbus_Disconnect.triggered.connect(self.modbus_bridge.stop_procedure)
        self.ui.actionService_Mode.triggered.connect(self.update_service_mode)
        self.ui.actionSave.triggered.connect(self.export_to_xlsx)
        self.ui.actionShow_legend_1.triggered.connect(lambda: self.legend_dialog.show())
        self.ui.actionRestart.triggered.connect(lambda: self.reset_device(hard_reset=True, immediately=True))
        self.ui.actionRestore_last_state.triggered.connect(self.restore_prev_session_callback)
        self.ui.actionSample_presence_test.triggered.connect(self.update_sample_presence_test_mode)

        # Controls connection
        self.ui.tray_left_out_pushButton.clicked.connect(lambda: self.move_tray_out(left=True))
        self.ui.tray_right_out_pushButton.clicked.connect(lambda: self.move_tray_out(left=False))
        self.ui.hard_reset_pushButton.clicked.connect(lambda: self.reset_device(hard_reset=True))
        self.ui.reset_goal_pushButton.clicked.connect(self.unset_target_position)
        self.ui.next_spot_PushButton.clicked.connect(self.next_target)
        self.ui.clean_trays_pushButton.clicked.connect(self.clean_trays)
        self.ui.exitPushButton.clicked.connect(self.closeEvent)
        self.ui.pass_pushButton.clicked.connect(self.dbs_proc_skip_spot)

        # Trigger init device scan
        self.ui.actionModbus_Scan.trigger()
        self.dbs_proc_thread.start()

        # Initialization
        self.update_last_state_log()

    def _init_scene(self):
        """
        Initialize Tray objects and set up graphic scene
        """
        self.left_tray = {}
        self.right_tray = {}
        self.left_graphic_scene = QGraphicsScene()
        self.right_graphic_scene = QGraphicsScene()
        self._init_plates()
        if self.mode in [DBSMode.LEFT, DBSMode.BOTH]:
            self.ui.left_graphicsView.setScene(self.left_graphic_scene)
        if self.mode in [DBSMode.BOTH, ]:
            self.ui.right_graphicsView.setScene(self.right_graphic_scene)

        # Disable unused Controls and Labels
        if self.mode is DBSMode.LEFT:
            self.ui.right_tray_name_lineEdit.setVisible(False)
            self.ui.right_graphicsView.setVisible(False)

    def _init_plates(self):
        """
        Init graphic scene - adding components to left and right trays, connecting the signals
        """
        horizontal = self.mode != DBSMode.BOTH
        self.left_spot_item_group = QSpotsItemGroup()
        self.right_spot_item_group = QSpotsItemGroup()
        for c in reversed(range(8)):
            for r in range(12):
                left_item = SpotGraphicItem(r, c, DBSTraySide.L, horizontal)
                right_item = SpotGraphicItem(r, c, DBSTraySide.R, horizontal)
                self.left_tray[(c, r)] = left_item
                self.right_tray[(c, r)] = right_item

                # Add spots to QGraphicsItemGroups
                self.left_spot_item_group.addToGroup(left_item)
                self.right_spot_item_group.addToGroup(right_item)

                # Connect signals
                left_item.emitter.spot_click_signal.connect(self.spot_click_slot)
                right_item.emitter.spot_click_signal.connect(self.spot_click_slot)
                left_item.emitter.log_trigger_signal.connect(self._log_state)
                right_item.emitter.log_trigger_signal.connect(self._log_state)

        # Add groups to the graphic scenes
        self.left_graphic_scene.addItem(self.left_spot_item_group)
        self.right_graphic_scene.addItem(self.right_spot_item_group)

    @staticmethod
    def _setup_logger(name: str, log_file: str, log_time=False):
        """
        Setup logger object
        """
        fh = RotatingFileHandler(log_file, maxBytes=int(1e6), backupCount=1)
        formatter_str = "%(asctime)-15s %(message)s" if log_time else "%(message)s"
        fh.setFormatter(logging.Formatter(formatter_str))

        logger = logging.getLogger(name)
        logger.setLevel(logging.INFO)
        logger.addHandler(fh)
        return logger

    def _log_state(self):
        """
        Uses logger to save current plate state
        """
        log_message = self._get_state_log()
        self.logger.info(log_message)

    def _log_debug_msg(self, log_message: str):
        """
        Uses logger to save debug message
        """
        if self.debug_logger is not None:
            self.debug_logger.info(f'DBS: {log_message}')

    # @staticmethod
    def _get_state_log_name(self, log_msg, encode=True):
        """
        Encode/decode logged sample name to get rid of the semicolon that is used as a separator

        :param encode: Encode the name if True, decode if False
        """
        # TODO: idk, this is super dumb, but I had no other ideas :/
        if encode:
            log_msg = log_msg.replace(',', '$._.')
            return log_msg.replace(';', '$:_.')
        else:
            log_msg = log_msg.replace('$._.', ',')
            return log_msg.replace('$:_.', ';')

    def _get_state_log(self):
        """
        Returns log message that contains full plate info
        """
        spots = list(self.left_tray.values()) + list(self.right_tray.values())
        message = ';'.join([
            f'{s.id[2].value},{s.id[0]},{s.id[1]},'
            f'{s.state.value if s.state in [SpotState.WITH_PUNCH, SpotState.BLOCK] else SpotState.EMPTY.value},'
            f'{self._get_state_log_name(s.name, encode=True)}' for s in spots
        ])
        return message

    def load_dbs_state(self, state_log: str):
        """
        Updates the current DBS trays state with the state saved in state log message.

        :param state_log: State log message
        """
        trays = {DBSTraySide.L.value: self.left_tray, DBSTraySide.R.value: self.right_tray}
        for spot_log in state_log.split(';'):
            spot_log_list = spot_log.split(',')
            side, column, row, state = map(int, spot_log_list[:4])
            spot = trays[side][(column, row)]
            spot.state = SpotState(state)
            spot.name = self._get_state_log_name(spot_log_list[4], encode=False)
            spot.update()

    def update_last_state_log(self):
        """
        Updates previous session log with the log message saved in the log file.
        """
        try:
            with open('./.dbs_app.log', 'r') as log_file:
                logs = log_file.readlines()
            self.last_state_log = logs[-1].rstrip() if logs else self._get_state_log()
        except Exception as e:
            QMessageBox.warning(self, "DBS Error",
                                f"An error occurred while processing .dbs_app.log file.\n"
                                f"MSG: {str(e)}", QMessageBox.Ok)

    def restore_prev_session_callback(self):
        """
        Restores state of the previous session saved in last_state_log.
        """
        if not self.check_trays_empty():
            ret = QMessageBox.warning(
                self, "Restore last session",
                f"The contents of the plates will be overwritten with the data from the previous session.\n"
                f"Are you sure?", QMessageBox.Yes | QMessageBox.No)
            if ret == QMessageBox.No:
                return
        try:
            self.load_dbs_state(self.last_state_log)
        except Exception as e:
            QMessageBox.warning(self, "DBS Error",
                                f"An error occurred while processing previous session state.\n"
                                f"MSG: {str(e)}", QMessageBox.Ok)
            self._log_debug_msg(f"Restore state: {str(e)}")
        else:
            QMessageBox.information(self, "DBS Info",
                                    f"Saved DBS session loaded", QMessageBox.Ok)
            self._log_debug_msg(f"Restore state: OK")

    @Slot(SpotGraphicItem)
    def spot_click_slot(self, spot: SpotGraphicItem):
        """
        Signal from single spot clicked, sent from SpotGraphicItem

        :param spot: Clicked spot
        """
        position = spot.id
        tray = self.left_tray if self.current_tray == DBSTraySide.L else self.right_tray
        if self.target_position and tray[self.target_position[:2]].is_goal() \
                and position[:2] != self.target_position:
            tray[self.target_position[:2]].reset()
        self.current_tray = position[2]
        self.target_position = position[:2]

    def dbs_proc_loop_thread(self):
        """
        Main DBS procedure. Runs cut procedure in position described by self.current_position in loop.
        """
        while not self.dbs_proc_stop_event.is_set():
            goal_pos = self.target_position
            goal_tray_code = self.current_tray
            goal_tray = self.left_tray if goal_tray_code == DBSTraySide.L else self.right_tray

            if self.dbs_proc_pause_event.is_set():
                if self.modbus_bridge.dbs_status in [DBSStatus.IDLE, DBSStatus.UNKNOWN]:
                    self._log_debug_msg('DPLT: Clear pause event')
                    self.dbs_proc_pause_event.clear()
            elif goal_pos and self.modbus_bridge.is_alive():
                # Ignore goal pos update if it was set outside main procedure
                if self.current_tray == goal_tray_code and self.target_position == goal_pos:
                    self.next_target()
                    # To make sure current pos is updated :/
                    while goal_pos == self.target_position:
                        time.sleep(0.01)

                # Spot processing
                self._log_debug_msg(f'DPLT: Start dbs_proc_cut; goal_pos: {goal_pos}')
                ret = self.dbs_proc_cut(goal_tray, goal_pos)
                self._log_debug_msg('DPLT: Finished dbs_proc_cut')

                # Reset sample name
                self.ui.sample_name_lineEdit.setText('')
                if self.dbs_proc_stop_event.is_set():
                    break
            else:
                time.sleep(0.1)

    def dbs_proc_cut(self, tray: dict, goal_pos: tuple):
        """
        DBS disc cut procedure

        :return: If procedure succeeded
        """
        self.dbs_proc_restore_state_action_enable_signal.emit(False)
        step = DBSCutProcStep.START
        spot = tray[goal_pos]
        success = False
        sample_name_set = False
        while step != DBSCutProcStep.END and not self.dbs_proc_stop_event.is_set() \
                and not self.dbs_proc_pause_event.is_set() and self.modbus_bridge.is_alive():
            # 1. --> Start
            if step is DBSCutProcStep.START:
                # Enable name edit and pass button
                if not self.dbs_service_mode and not sample_name_set:
                    self.dbs_proc_name_edit_enable_signal.emit(True)
                    self.dbs_proc_pass_enable_signal.emit(True)
                self.dbs_proc_update_spot_signal.emit(spot, SpotState.PROCESS)
                step = DBSCutProcStep.MOVE
                self._log_debug_msg(f'DPC: Start; Next step: {step.name};  Spot: {spot.state.name}')
            # 2. --> Move over target position
            elif step is DBSCutProcStep.MOVE and self.modbus_bridge.dbs_status == DBSStatus.IDLE:
                self.modbus_bridge.dequeue_cmd_move(goal_pos[0], goal_pos[1])
                step = DBSCutProcStep.NAME
                self._log_debug_msg(f'DPC: Move; Next step: {step.name}')
            # 3. --> Wait for sample name and send cut command
            elif step is DBSCutProcStep.NAME:
                # Pass processing
                if self.dbs_proc_skip_event.is_set():
                    step = DBSCutProcStep.END
                    self._log_debug_msg(f'DPC: Skipped! Next step: {step.name}')
                # Update name and cut
                elif self.dbs_sample_name or self.dbs_service_mode:
                    spot.name = self.dbs_sample_name if not self.dbs_service_mode else 'OK'
                    sample_name_set = True
                    self.dbs_proc_reset_tray_buttons_enable_signal.emit(False)
                    self.dbs_proc_pass_enable_signal.emit(False)
                    self.dbs_proc_name_edit_enable_signal.emit(False)
                    self.modbus_bridge.dequeue_cmd_cut()
                    step = DBSCutProcStep.MOVE_WAIT
                    self._log_debug_msg(f'DPC: Next step: {step.name}; sample_name: {self.dbs_sample_name}; Spot: {spot.state.name}')
            # 4. --> Wait until cut command is executed
            elif step is DBSCutProcStep.MOVE_WAIT and self.modbus_bridge.last_cmd == DBSCommand.CUT \
                    and self.modbus_bridge.dbs_status == DBSStatus.BUSY:
                self.dbs_proc_update_spot_signal.emit(spot, SpotState.UNDER_CUTTER)
                step = DBSCutProcStep.CUT
                self._log_debug_msg(f'DPC: Next step: {step.name};  Spot: {spot.state.name}')
            # 5.0 --> If cut succeeded
            elif step is DBSCutProcStep.CUT and self.modbus_bridge.dbs_status is DBSStatus.IDLE:
                self.dbs_proc_update_spot_signal.emit(spot, SpotState.WITH_PUNCH)
                step = DBSCutProcStep.END
                success = True
                self._log_debug_msg(f'DPC: OK: Next step: {step.name};  Spot: {spot.state.name}')
            # 5.1 --> If not, check if retry
            elif step is DBSCutProcStep.CUT and self.modbus_bridge.dbs_status is DBSStatus.ERROR:
                self.dbs_proc_update_spot_signal.emit(spot, SpotState.PROCESS)
                # Show error dialog
                time.sleep(2)
                self.dbs_proc_dialog_show_event.set()
                self.dbs_proc_error_dialog_signal.emit(f'{chr(65 + goal_pos[0])}{goal_pos[1] + 1}')
                self._log_debug_msg(f'DPC: Error;  Spot: {spot.state.name}')
                # Wait for user's decision
                while self.dbs_proc_dialog_show_event.is_set():
                    time.sleep(0.5)
                if not self.dbs_proc_error_ignore:  # Retry cut
                    step = DBSCutProcStep.MOVE
                    self.dbs_proc_update_spot_signal.emit(spot, SpotState.CUTTER_GOAL)
                else:  # Ignore
                    self.dbs_proc_update_spot_signal.emit(spot, SpotState.WITH_PUNCH)
                    step = DBSCutProcStep.END
                    success = True
                # Reset error state
                time.sleep(0.5)
                self.modbus_bridge.write_cmd_reset(hard_reset=False, immediately=True)
                self._log_debug_msg(f'DPC: Next step: {step.name};  Spot: {spot.state.name}')
            # If pause event is set (Tray out)
            if self.dbs_proc_pause_event.is_set():
                step = DBSCutProcStep.END
                self._log_debug_msg('DPC: Exit loop: Pause event was set!')
            else:
                time.sleep(0.05)

        self.dbs_proc_reset_tray_buttons_enable_signal.emit(True)
        # for safety
        self.dbs_proc_pass_enable_signal.emit(False)
        self.dbs_proc_name_edit_enable_signal.emit(False)
        self.dbs_proc_restore_state_action_enable_signal.emit(True)
        if not spot.is_processing_finished():
            self._log_debug_msg('DPC: Spot reset. Processing unfinished')
            self.dbs_proc_reset_spot_signal.emit(spot)

        if self.dbs_proc_pause_event.is_set():
            success = True
        return success

    def scan_for_devices(self):
        """
        Scan for available modbus devices.
        """
        prev_selected = self.modbus_devices_ag.checkedAction()

        # remove old actions and menu items
        for action in self.modbus_devices_ag.actions():
            self.modbus_devices_ag.removeAction(action)
        for action in self.modbus_devices_menu.actions():
            self.modbus_devices_menu.removeAction(action)

        # Update menu with the new devices
        self.modbus_bridge.scan_for_devices()
        for dev_name, dev in self.modbus_bridge.devices_dict.items():
            a = self.modbus_devices_ag.addAction(QAction(dev_name, checkable=True))
            self.modbus_devices_menu.addAction(a)

        # Check previously selected action
        if prev_selected is not None:
            for action in self.ui.actionModbus_Connect.menu().actions():
                if action.text() == prev_selected.text():
                    action.setChecked(True)
                    break

    def connect_device(self):
        """
        Connects to modbus device
        """
        self.unset_target_position()
        selected = self.modbus_devices_ag.checkedAction()
        self.modbus_bridge.connect_to_modbus(selected.text())
        if self.modbus_bridge.is_alive():
            self.ui.actionDev_serial.setText(f'Dev serial: {self.modbus_bridge.dbs_serial}')
        else:
            selected.setChecked(False)
            self.ui.actionDev_serial.setText('Device serial')

    @Slot(int)
    def update_modbus_status(self, status: int):
        """
        Updating modbus status
        :param bool status: status of modbus
        """
        self.ui.modbus_status.setPixmap(self.pixmaps[get_color(status)])
        self.ui.actionModbus_Disconnect.setEnabled(status == 1)
        if status != 1:
            checked = self.modbus_devices_ag.checkedAction()
            if checked is not None:
                checked.setChecked(False)
                self.ui.actionDev_serial.setText('Device serial')

    @Slot(int)
    def update_dbs_status(self, status: int):
        """
        Updating modbus status
        :param bool status: status of modbus
        """
        self.ui.dbs_status.setPixmap(self.pixmaps[get_color(status)])

    @Slot(tuple)
    def update_dbs_position(self, pos: tuple):
        """
        Updating DBS position
        :param pos: Current position (c, r, t)
        """
        pos_txt = f'{chr(65 + pos[0])}{pos[1] + 1}' if pos[:2] != (-1, -1) else '?'
        self.ui.position_status.setText(f'{pos[2]} {pos_txt}')

    def check_trays_empty(self):
        """
        Check if trays are empty. Return False if there is at least one blocked spot or spot with punch
        """
        left_f = [sp for sp in self.left_tray.values() if sp.is_processing_finished() or sp.is_blocked()]
        right_f = [sp for sp in self.right_tray.values() if sp.is_processing_finished() or sp.is_blocked()]
        return len(left_f) + len(right_f) == 0

    def find_next_target(self, position):
        """
        Search for the next goal position

        :param position: Current position
        :return: Position (c, r)
        """
        c, r = position
        if self.ui.mode_comboBox.currentIndex() == 0:
            c += 1
            if c > 7:
                r += 1
                c = 0
        else:
            r += 1
            if r % 12 == 0:
                c += 1
            r = r % 12
        if c > 7 or r > 11:
            return None
        return c, r

    def next_target(self):
        """
        Go to the next position
        """
        pos = self.target_position
        if not pos:
            return

        tray = self.left_tray if self.current_tray == DBSTraySide.L else self.right_tray
        while pos and not tray[pos].is_empty() or self.target_position == pos:
            pos = self.find_next_target(pos)
        if not pos:
            self.unset_target_position()
            return

        self.dbs_proc_update_spot_signal.emit(tray[pos], SpotState.CUTTER_GOAL)
        self.dbs_proc_spot_click_signal.emit(tray[pos])
        self._log_debug_msg(f'New target: {pos}')

    def dbs_proc_enable_pass(self, enable=True):
        """
        Enable/disable pass button. Clear dbs_proc_pass event.
        """
        self.ui.pass_pushButton.setEnabled(enable)
        self.dbs_proc_skip_event.clear()

    @Slot(bool)
    def dbs_proc_enable_name_edit(self, enable: bool):
        """
        Enables sample name edit and set dbs_sample_name_set flag to False
        """
        self.ui.sample_name_lineEdit.setEnabled(enable)
        if enable:
            self.dbs_sample_name = ''
            self.ui.sample_name_lineEdit.setFocus()

    @Slot()
    def dbs_proc_enable_reset_tray_buttons(self, enable=False):
        """
        Enable/disable buttons: Clean Tray, Reset, Try Out, according to dbs_proc_block_reset_event state
        """
        self.ui.hard_reset_pushButton.setEnabled(enable)
        self.ui.tray_left_out_pushButton.setEnabled(enable)
        self.ui.tray_right_out_pushButton.setEnabled(enable)
        self.ui.clean_trays_pushButton.setEnabled(enable)

    @Slot(bool)
    def dbs_proc_enable_restore_state_action(self, enable):
        """
        Enable/disable restore_last_state action
        """
        self.ui.actionRestore_last_state.setEnabled(enable)

    @Slot(str)
    def dbs_proc_ignore_error_dialog(self, slot_code):
        """
        Shows procedure error dialog. Updates self.

        :return: If error should be ignore
        """
        qm = QMessageBox
        reply = qm.question(self, "DBS Error",
                            f"An error was detected.\nPlease check if the disc was cut out: {slot_code}\nGo forward?",
                            qm.Yes | qm.No)
        self.dbs_proc_error_ignore = reply == qm.Yes
        self.dbs_proc_dialog_show_event.clear()

    def dbs_proc_skip_spot(self):
        """
        Pass processing of the current slot
        """
        if not self.dbs_service_mode:
            self.dbs_proc_skip_event.set()
        self.ui.pass_pushButton.setEnabled(False)

    def reset_device(self, hard_reset=False, immediately=False):
        """
        DBS pause. Resets DBS and sets dbs_proc_pause_event flag.
        """
        self._log_debug_msg(f'Reset:  hard_reset: {hard_reset};  immediately: {immediately}')
        self.unset_target_position()
        self.dbs_proc_pause_event.set()
        self.modbus_bridge.write_cmd_reset(hard_reset, immediately=immediately)

    def unset_target_position(self):
        """
        Clean status of goal spot, sets current_position to None
        """
        for spot in list(self.left_tray.values()) + list(self.right_tray.values()):
            if spot.is_goal() and spot.id[:2]:
                self.dbs_proc_reset_spot_signal.emit(spot)
        self.target_position = None

    def clean_trays(self):
        """
        Clean trays
        """
        qm = QMessageBox
        ret = qm.warning(self, 'Clean Plate', "Plates content will be erased. \nAre you sure?", qm.Yes | qm.No)
        if ret == qm.No:
            return

        # Export data
        qm = QMessageBox
        ret = qm.question(self, 'Clean Plate',
                          "Plates content will be erased. \nDo you want to export the plate first?",
                          qm.Yes | qm.No)
        if ret == qm.Yes:
            self.export_to_xlsx()

        # Clean plate
        self._log_debug_msg('Clean trays')
        self.unset_target_position()
        self.dbs_proc_pause_event.set()
        self.update_last_state_log()
        for l_spot, r_spot in zip(self.left_tray.values(), self.right_tray.values()):
            l_spot.reset()
            r_spot.reset()

    def update_service_mode(self):
        """
        Check if DBS controller should operate in service mode and set service_mode flag
        """
        self.dbs_service_mode = self.ui.actionService_Mode.isChecked()
        self._log_debug_msg(f'dbs_service_mode: {self.dbs_service_mode}')

    def update_sample_presence_test_mode(self):
        """
        Check if DBS should test each spot for a sample
        """
        self.dbs_sample_presence_test_mode = self.ui.actionSample_presence_test.isChecked()
        self.modbus_bridge.enable_spot_sensor(self.dbs_sample_presence_test_mode)
        self._log_debug_msg(f'sample_presence_test: {self.dbs_sample_presence_test_mode}')

    def move_tray_out(self, left=True):
        """
        Move tray out to the left or right
        """
        self.unset_target_position()
        self.dbs_proc_pause_event.set()
        drc = 'L' if left else 'R'
        self._log_debug_msg(f'Move tray out: {drc}')
        self.modbus_bridge.dequeue_cmd_tray('O', drc)

    def export_to_xlsx(self):
        """
        Exports plate data to xlsx spreadsheet
        """
        # TODO - this works only for the left tray
        path = QFileDialog.getSaveFileName(self, 'Save File', '', 'XLSX files (*.xlsx)',
                                           options=self._dialog_options)[0]
        if path:
            path = path if path.endswith('.xlsx') else path + '.xlsx'
            plate = self.left_tray
            save_plate_spreadsheet(path, plate, self.ui.left_tray_name_lineEdit.text(),
                                   self.ui.user_name_lineEdit.text())

    def keyPressEvent(self, event):
        """
        Go to the next position on enter
        :param event: QtEvent
        """
        if event.type() == QEvent.KeyPress and (event.key() == Qt.Key_Enter or event.key() == Qt.Key_Return):
            if self.ui.sample_name_lineEdit.isEnabled():
                text = self.ui.sample_name_lineEdit.text()
                if text:
                    # self.target_position = None
                    self.dbs_sample_name = text
                    self.dbs_proc_name_edit_enable_signal.emit(False)
                    self._log_debug_msg('Spot name accepted')

    def closeEvent(self, event):
        """
        Closing
        :param event: qt event
        """
        if not self.modbus_bridge:
            sys.exit()
        if not self.check_trays_empty():
            qm = QMessageBox
            ret = qm.warning(self, 'Close',
                             "DBS Controller will be closed. \nAre you sure?",
                             qm.Yes | qm.No)
            if ret == qm.No:
                if isinstance(event, QEvent):
                    event.ignore()
                return
        self.modbus_bridge.stop_procedure()
        self.dbs_proc_stop_event.set()

        # Export data
        if not self.check_trays_empty():
            qm = QMessageBox
            ret = qm.question(self, 'Close',
                              "DBS Controller will be closed. \nDo you want to export the plate first?",
                              qm.Yes | qm.No)
            if ret == qm.Yes:
                self.export_to_xlsx()
        sys.exit()

    def resizeEvent(self, event):
        w, h = self.size().width() - self.tool_width, self.size().height()
        # Set scale according to DBS app mode
        scale = min(w / 1000 / 2, h / 1400) if self.mode == DBSMode.BOTH else min(w / 1400, h / 1000)

        # Scale plate
        cx, cy = self.ui.left_graphicsView.width() // 2, self.ui.left_graphicsView.height() // 2
        self.left_spot_item_group.setScale(scale)

        # Center plate
        ig_size = self.left_spot_item_group.mapToScene(self.left_spot_item_group.boundingRect().center())
        cgx, cgy = ig_size.x(), ig_size.y()
        dx, dy = int(cx - cgx), int(cy - cgy)

        self.left_spot_item_group.moveBy(dx, dy)
        self.left_graphic_scene.setSceneRect(0, 0, self.ui.left_graphicsView.size().width(),
                                             self.ui.left_graphicsView.size().height())
        if self.mode == DBSMode.BOTH:
            # Apply the same transforms to the right plate
            self.right_spot_item_group.setScale(scale)
            self.right_spot_item_group.moveBy(dx, dy)
            self.right_graphic_scene.setSceneRect(0, 0, self.ui.right_graphicsView.size().width(),
                                                  self.ui.right_graphicsView.size().height())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle("fusion")
    mainWindow = DryBloodSystem()
    mainWindow.show()
    sys.exit(app.exec_())
