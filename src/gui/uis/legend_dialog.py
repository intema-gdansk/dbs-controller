# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'legend_dialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from gui.uis import resources_rc
from gui.uis import resources_rc

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(593, 229)
        icon = QIcon()
        icon.addFile(u":/icons/icons/Red_blood_cell.png", QSize(), QIcon.Normal, QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 6, 0, 0)
        self.gridLayout = QGridLayout()
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.widget_3 = QWidget(Dialog)
        self.widget_3.setObjectName(u"widget_3")
        self.formLayout_3 = QFormLayout(self.widget_3)
        self.formLayout_3.setObjectName(u"formLayout_3")
        self.dbs_label_5 = QLabel(self.widget_3)
        self.dbs_label_5.setObjectName(u"dbs_label_5")
        self.dbs_label_5.setMinimumSize(QSize(25, 25))
        self.dbs_label_5.setMaximumSize(QSize(25, 25))
        self.dbs_label_5.setStyleSheet(u"")
        self.dbs_label_5.setPixmap(QPixmap(u":/icons/icons/Green_Light_Icon.svg"))
        self.dbs_label_5.setScaledContents(True)

        self.formLayout_3.setWidget(1, QFormLayout.LabelRole, self.dbs_label_5)

        self.label_14 = QLabel(self.widget_3)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setMinimumSize(QSize(0, 25))
        self.label_14.setMaximumSize(QSize(16777215, 25))

        self.formLayout_3.setWidget(1, QFormLayout.FieldRole, self.label_14)

        self.dbs_label_7 = QLabel(self.widget_3)
        self.dbs_label_7.setObjectName(u"dbs_label_7")
        self.dbs_label_7.setMinimumSize(QSize(25, 25))
        self.dbs_label_7.setMaximumSize(QSize(25, 25))
        self.dbs_label_7.setPixmap(QPixmap(u":/icons/icons/Red_Light_Icon.svg"))
        self.dbs_label_7.setScaledContents(True)

        self.formLayout_3.setWidget(3, QFormLayout.LabelRole, self.dbs_label_7)

        self.label_15 = QLabel(self.widget_3)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setMinimumSize(QSize(0, 25))
        self.label_15.setMaximumSize(QSize(16777215, 25))

        self.formLayout_3.setWidget(3, QFormLayout.FieldRole, self.label_15)

        self.dbs_label_8 = QLabel(self.widget_3)
        self.dbs_label_8.setObjectName(u"dbs_label_8")
        self.dbs_label_8.setMinimumSize(QSize(25, 25))
        self.dbs_label_8.setMaximumSize(QSize(25, 25))
        self.dbs_label_8.setPixmap(QPixmap(u":/icons/icons/Gray_Light_Icon.svg"))
        self.dbs_label_8.setScaledContents(True)

        self.formLayout_3.setWidget(5, QFormLayout.LabelRole, self.dbs_label_8)

        self.label_16 = QLabel(self.widget_3)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setMinimumSize(QSize(0, 25))
        self.label_16.setMaximumSize(QSize(16777215, 25))

        self.formLayout_3.setWidget(5, QFormLayout.FieldRole, self.label_16)


        self.gridLayout.addWidget(self.widget_3, 1, 2, 1, 1)

        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        self.formLayout = QFormLayout(self.widget)
        self.formLayout.setObjectName(u"formLayout")
        self.spot_label_1 = QLabel(self.widget)
        self.spot_label_1.setObjectName(u"spot_label_1")
        self.spot_label_1.setMinimumSize(QSize(25, 25))
        self.spot_label_1.setMaximumSize(QSize(25, 25))
        self.spot_label_1.setStyleSheet(u"background: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.493072, fx:0.5, fy:0.5, stop:0.653465 rgba(255, 255, 255, 232), stop:1 rgba(255, 255, 255, 0))")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.spot_label_1)

        self.label_2 = QLabel(self.widget)
        self.label_2.setObjectName(u"label_2")

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.label_2)

        self.spot_label_2 = QLabel(self.widget)
        self.spot_label_2.setObjectName(u"spot_label_2")
        self.spot_label_2.setMinimumSize(QSize(25, 25))
        self.spot_label_2.setMaximumSize(QSize(25, 25))
        self.spot_label_2.setStyleSheet(u"background: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.493072, fx:0.5, fy:0.5, stop:0.653465 rgba(255, 255, 0, 232), stop:1 rgba(255, 255, 255, 0))")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.spot_label_2)

        self.label_4 = QLabel(self.widget)
        self.label_4.setObjectName(u"label_4")

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.label_4)

        self.spot_label_3 = QLabel(self.widget)
        self.spot_label_3.setObjectName(u"spot_label_3")
        self.spot_label_3.setMinimumSize(QSize(25, 25))
        self.spot_label_3.setMaximumSize(QSize(25, 25))
        self.spot_label_3.setStyleSheet(u"background: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.493072, fx:0.5, fy:0.5, stop:0.653465 rgba(200, 255, 0, 232), stop:1 rgba(255, 255, 255, 0))")

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.spot_label_3)

        self.label_6 = QLabel(self.widget)
        self.label_6.setObjectName(u"label_6")

        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.label_6)

        self.spot_label_4 = QLabel(self.widget)
        self.spot_label_4.setObjectName(u"spot_label_4")
        self.spot_label_4.setMinimumSize(QSize(25, 25))
        self.spot_label_4.setMaximumSize(QSize(25, 25))
        self.spot_label_4.setStyleSheet(u"background: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.493072, fx:0.5, fy:0.5, stop:0.653465 rgba(0, 0, 240, 232), stop:1 rgba(255, 255, 255, 0))")

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.spot_label_4)

        self.label = QLabel(self.widget)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.label)

        self.spot_label_5 = QLabel(self.widget)
        self.spot_label_5.setObjectName(u"spot_label_5")
        self.spot_label_5.setMinimumSize(QSize(25, 25))
        self.spot_label_5.setMaximumSize(QSize(25, 25))
        self.spot_label_5.setStyleSheet(u"background: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.493072, fx:0.5, fy:0.5, stop:0.653465 rgba(240, 0, 0, 1), stop:1 rgba(255, 255, 255, 0))")

        self.formLayout.setWidget(5, QFormLayout.LabelRole, self.spot_label_5)

        self.label_3 = QLabel(self.widget)
        self.label_3.setObjectName(u"label_3")

        self.formLayout.setWidget(5, QFormLayout.FieldRole, self.label_3)

        self.spot_label_6 = QLabel(self.widget)
        self.spot_label_6.setObjectName(u"spot_label_6")
        self.spot_label_6.setMinimumSize(QSize(25, 25))
        self.spot_label_6.setMaximumSize(QSize(25, 25))
        self.spot_label_6.setStyleSheet(u"background: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.493072, fx:0.5, fy:0.5, stop:0.653465 rgba(40, 40, 60, 232), stop:1 rgba(255, 255, 255, 0))")

        self.formLayout.setWidget(6, QFormLayout.LabelRole, self.spot_label_6)

        self.label_7 = QLabel(self.widget)
        self.label_7.setObjectName(u"label_7")

        self.formLayout.setWidget(6, QFormLayout.FieldRole, self.label_7)


        self.gridLayout.addWidget(self.widget, 1, 0, 1, 1)

        self.widget_2 = QWidget(Dialog)
        self.widget_2.setObjectName(u"widget_2")
        self.formLayout_2 = QFormLayout(self.widget_2)
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.dbs_label_1 = QLabel(self.widget_2)
        self.dbs_label_1.setObjectName(u"dbs_label_1")
        self.dbs_label_1.setMinimumSize(QSize(25, 25))
        self.dbs_label_1.setMaximumSize(QSize(25, 25))
        self.dbs_label_1.setStyleSheet(u"")
        self.dbs_label_1.setPixmap(QPixmap(u":/icons/icons/Green_Light_Icon.svg"))
        self.dbs_label_1.setScaledContents(True)

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole, self.dbs_label_1)

        self.label_8 = QLabel(self.widget_2)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMinimumSize(QSize(0, 25))
        self.label_8.setMaximumSize(QSize(16777215, 25))

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole, self.label_8)

        self.dbs_label_2 = QLabel(self.widget_2)
        self.dbs_label_2.setObjectName(u"dbs_label_2")
        self.dbs_label_2.setMinimumSize(QSize(25, 25))
        self.dbs_label_2.setMaximumSize(QSize(25, 25))
        self.dbs_label_2.setPixmap(QPixmap(u":/icons/icons/Yellow_Light_Icon.svg"))
        self.dbs_label_2.setScaledContents(True)

        self.formLayout_2.setWidget(3, QFormLayout.LabelRole, self.dbs_label_2)

        self.label_9 = QLabel(self.widget_2)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setMinimumSize(QSize(0, 25))
        self.label_9.setMaximumSize(QSize(16777215, 25))

        self.formLayout_2.setWidget(3, QFormLayout.FieldRole, self.label_9)

        self.dbs_label_3 = QLabel(self.widget_2)
        self.dbs_label_3.setObjectName(u"dbs_label_3")
        self.dbs_label_3.setMinimumSize(QSize(25, 25))
        self.dbs_label_3.setMaximumSize(QSize(25, 25))
        self.dbs_label_3.setPixmap(QPixmap(u":/icons/icons/Red_Light_Icon.svg"))
        self.dbs_label_3.setScaledContents(True)

        self.formLayout_2.setWidget(5, QFormLayout.LabelRole, self.dbs_label_3)

        self.label_10 = QLabel(self.widget_2)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setMinimumSize(QSize(0, 25))
        self.label_10.setMaximumSize(QSize(16777215, 25))

        self.formLayout_2.setWidget(5, QFormLayout.FieldRole, self.label_10)

        self.dbs_label_4 = QLabel(self.widget_2)
        self.dbs_label_4.setObjectName(u"dbs_label_4")
        self.dbs_label_4.setMinimumSize(QSize(25, 25))
        self.dbs_label_4.setMaximumSize(QSize(25, 25))
        self.dbs_label_4.setPixmap(QPixmap(u":/icons/icons/Gray_Light_Icon.svg"))
        self.dbs_label_4.setScaledContents(True)

        self.formLayout_2.setWidget(7, QFormLayout.LabelRole, self.dbs_label_4)

        self.label_11 = QLabel(self.widget_2)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setMinimumSize(QSize(0, 25))
        self.label_11.setMaximumSize(QSize(16777215, 25))

        self.formLayout_2.setWidget(7, QFormLayout.FieldRole, self.label_11)


        self.gridLayout.addWidget(self.widget_2, 1, 1, 1, 1)

        self.label_12 = QLabel(Dialog)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_12, 0, 1, 1, 1)

        self.label_5 = QLabel(Dialog)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_5, 0, 0, 1, 1)

        self.label_13 = QLabel(Dialog)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_13, 0, 2, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Legend", None))
        self.dbs_label_5.setText("")
        self.label_14.setText(QCoreApplication.translate("Dialog", u"Online", None))
        self.dbs_label_7.setText("")
        self.label_15.setText(QCoreApplication.translate("Dialog", u"Offline", None))
        self.dbs_label_8.setText("")
        self.label_16.setText(QCoreApplication.translate("Dialog", u"Unknown", None))
        self.spot_label_1.setText("")
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Empty", None))
        self.spot_label_2.setText("")
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Next cutter target", None))
        self.spot_label_3.setText("")
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Currently processed", None))
        self.spot_label_4.setText("")
        self.label.setText(QCoreApplication.translate("Dialog", u"Waiting for the cut", None))
        self.spot_label_5.setText("")
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Sample inside", None))
        self.spot_label_6.setText("")
        self.label_7.setText(QCoreApplication.translate("Dialog", u"Blocked", None))
        self.dbs_label_1.setText("")
        self.label_8.setText(QCoreApplication.translate("Dialog", u"Idle", None))
        self.dbs_label_2.setText("")
        self.label_9.setText(QCoreApplication.translate("Dialog", u"Busy", None))
        self.dbs_label_3.setText("")
        self.label_10.setText(QCoreApplication.translate("Dialog", u"Error", None))
        self.dbs_label_4.setText("")
        self.label_11.setText(QCoreApplication.translate("Dialog", u"Unknown", None))
        self.label_12.setText(QCoreApplication.translate("Dialog", u"Device status:", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Spot status:", None))
        self.label_13.setText(QCoreApplication.translate("Dialog", u"Modbus status:", None))
    # retranslateUi

