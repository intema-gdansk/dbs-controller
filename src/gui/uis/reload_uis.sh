pyside2-rcc resources_rc.qrc -o resources_rc.py
pyside2-uic working_gui.ui -o working_gui.py
pyside2-uic legend_dialog.ui -o legend_dialog.py


out_var="from gui.uis import resources_rc"
in_var="import resources_rc_rc"

sed -i -e "s/$in_var/$out_var/g" working_gui.py
sed -i -e "s/$in_var/$out_var/g" legend_dialog.py
