# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'working_gui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from gui.uis import resources_rc

class Ui_DryBloodWindow(object):
    def setupUi(self, DryBloodWindow):
        if not DryBloodWindow.objectName():
            DryBloodWindow.setObjectName(u"DryBloodWindow")
        DryBloodWindow.resize(1100, 670)
        DryBloodWindow.setMinimumSize(QSize(900, 550))
        DryBloodWindow.setMaximumSize(QSize(16777215, 16777215))
        self.actionModbus_Scan = QAction(DryBloodWindow)
        self.actionModbus_Scan.setObjectName(u"actionModbus_Scan")
        icon = QIcon()
        icon.addFile(u":/icons/icons/bikini60s_search.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.actionModbus_Scan.setIcon(icon)
        self.actionExit = QAction(DryBloodWindow)
        self.actionExit.setObjectName(u"actionExit")
        icon1 = QIcon()
        icon1.addFile(u":/icons/icons/bikini60s_image.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.actionExit.setIcon(icon1)
        self.actionSettings = QAction(DryBloodWindow)
        self.actionSettings.setObjectName(u"actionSettings")
        icon2 = QIcon()
        icon2.addFile(u":/icons/icons/bikini60s_settings.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.actionSettings.setIcon(icon2)
        self.actionModbus_Connect = QAction(DryBloodWindow)
        self.actionModbus_Connect.setObjectName(u"actionModbus_Connect")
        icon3 = QIcon()
        icon3.addFile(u":/icons/icons/bikini60s_archive.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.actionModbus_Connect.setIcon(icon3)
        self.actionModbus_Disconnect = QAction(DryBloodWindow)
        self.actionModbus_Disconnect.setObjectName(u"actionModbus_Disconnect")
        self.actionModbus_Disconnect.setEnabled(False)
        icon4 = QIcon()
        icon4.addFile(u":/icons/icons/disconnect.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.actionModbus_Disconnect.setIcon(icon4)
        self.actionSave = QAction(DryBloodWindow)
        self.actionSave.setObjectName(u"actionSave")
        icon5 = QIcon()
        icon5.addFile(u":/icons/icons/bikini60s_download.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.actionSave.setIcon(icon5)
        self.actionService_Mode = QAction(DryBloodWindow)
        self.actionService_Mode.setObjectName(u"actionService_Mode")
        self.actionService_Mode.setCheckable(True)
        self.actionDev_serial = QAction(DryBloodWindow)
        self.actionDev_serial.setObjectName(u"actionDev_serial")
        self.actionDev_serial.setIcon(icon2)
        self.actionEmpty = QAction(DryBloodWindow)
        self.actionEmpty.setObjectName(u"actionEmpty")
        self.actionShow_legend_1 = QAction(DryBloodWindow)
        self.actionShow_legend_1.setObjectName(u"actionShow_legend_1")
        icon6 = QIcon()
        icon6.addFile(u":/icons/icons/bikini60s_book.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.actionShow_legend_1.setIcon(icon6)
        self.actionRestart = QAction(DryBloodWindow)
        self.actionRestart.setObjectName(u"actionRestart")
        icon7 = QIcon()
        icon7.addFile(u":/icons/icons/reset.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.actionRestart.setIcon(icon7)
        self.actionRestore_last_state = QAction(DryBloodWindow)
        self.actionRestore_last_state.setObjectName(u"actionRestore_last_state")
        icon8 = QIcon()
        icon8.addFile(u":/icons/icons/bikini60s_upload.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.actionRestore_last_state.setIcon(icon8)
        self.actionSensor_sample_test = QAction(DryBloodWindow)
        self.actionSensor_sample_test.setObjectName(u"actionSensor_sample_test")
        self.actionSensor_sample_test.setCheckable(True)
        self.actionSample_presence_test = QAction(DryBloodWindow)
        self.actionSample_presence_test.setObjectName(u"actionSample_presence_test")
        self.actionSample_presence_test.setCheckable(True)
        self.actionSample_presence_test.setEnabled(False)
        self.actionSample_presence_test.setVisible(False)
        self.centralwidget = QWidget(DryBloodWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.left_graphicsView = QGraphicsView(self.centralwidget)
        self.left_graphicsView.setObjectName(u"left_graphicsView")
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.left_graphicsView.sizePolicy().hasHeightForWidth())
        self.left_graphicsView.setSizePolicy(sizePolicy)
        self.left_graphicsView.setAutoFillBackground(False)
        self.left_graphicsView.setStyleSheet(u"")
        self.left_graphicsView.setFrameShape(QFrame.NoFrame)
        self.left_graphicsView.setFrameShadow(QFrame.Plain)
        self.left_graphicsView.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.left_graphicsView.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.horizontalLayout.addWidget(self.left_graphicsView)

        self.right_graphicsView = QGraphicsView(self.centralwidget)
        self.right_graphicsView.setObjectName(u"right_graphicsView")
        sizePolicy.setHeightForWidth(self.right_graphicsView.sizePolicy().hasHeightForWidth())
        self.right_graphicsView.setSizePolicy(sizePolicy)
        self.right_graphicsView.setFrameShape(QFrame.NoFrame)
        self.right_graphicsView.setFrameShadow(QFrame.Plain)
        self.right_graphicsView.setSizeAdjustPolicy(QAbstractScrollArea.AdjustIgnored)
        self.right_graphicsView.setResizeAnchor(QGraphicsView.NoAnchor)

        self.horizontalLayout.addWidget(self.right_graphicsView)

        self.tool_widget = QWidget(self.centralwidget)
        self.tool_widget.setObjectName(u"tool_widget")
        self.tool_widget.setMinimumSize(QSize(220, 500))
        self.tool_widget.setMaximumSize(QSize(220, 840))
        self.verticalLayout_2 = QVBoxLayout(self.tool_widget)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.user_widget = QWidget(self.tool_widget)
        self.user_widget.setObjectName(u"user_widget")
        self.horizontalLayout_2 = QHBoxLayout(self.user_widget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 6)
        self.user_label = QLabel(self.user_widget)
        self.user_label.setObjectName(u"user_label")

        self.horizontalLayout_2.addWidget(self.user_label)

        self.user_name_lineEdit = QLineEdit(self.user_widget)
        self.user_name_lineEdit.setObjectName(u"user_name_lineEdit")

        self.horizontalLayout_2.addWidget(self.user_name_lineEdit)


        self.verticalLayout_2.addWidget(self.user_widget)

        self.status_widget = QWidget(self.tool_widget)
        self.status_widget.setObjectName(u"status_widget")
        self.gridLayout = QGridLayout(self.status_widget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 6, 0)
        self.modbus_label = QLabel(self.status_widget)
        self.modbus_label.setObjectName(u"modbus_label")

        self.gridLayout.addWidget(self.modbus_label, 0, 1, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer, 0, 3, 1, 1)

        self.dbs_status = QLabel(self.status_widget)
        self.dbs_status.setObjectName(u"dbs_status")
        self.dbs_status.setMinimumSize(QSize(25, 25))
        self.dbs_status.setMaximumSize(QSize(25, 25))
        self.dbs_status.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.dbs_status, 1, 5, 1, 1)

        self.horizontalSpacer_6 = QSpacerItem(70, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_6, 1, 3, 1, 1)

        self.dbs_label = QLabel(self.status_widget)
        self.dbs_label.setObjectName(u"dbs_label")

        self.gridLayout.addWidget(self.dbs_label, 1, 1, 1, 1)

        self.modbus_status = QLabel(self.status_widget)
        self.modbus_status.setObjectName(u"modbus_status")
        self.modbus_status.setMinimumSize(QSize(25, 25))
        self.modbus_status.setMaximumSize(QSize(25, 25))
        self.modbus_status.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.modbus_status, 0, 5, 1, 1)


        self.verticalLayout_2.addWidget(self.status_widget)

        self.position_horizontalLayout = QHBoxLayout()
        self.position_horizontalLayout.setObjectName(u"position_horizontalLayout")
        self.position_label = QLabel(self.tool_widget)
        self.position_label.setObjectName(u"position_label")

        self.position_horizontalLayout.addWidget(self.position_label)

        self.horizontalSpacer_4 = QSpacerItem(20, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.position_horizontalLayout.addItem(self.horizontalSpacer_4)

        self.position_status = QLabel(self.tool_widget)
        self.position_status.setObjectName(u"position_status")
        self.position_status.setMinimumSize(QSize(50, 0))
        self.position_status.setAlignment(Qt.AlignCenter)

        self.position_horizontalLayout.addWidget(self.position_status)


        self.verticalLayout_2.addLayout(self.position_horizontalLayout)

        self.trays_widget = QWidget(self.tool_widget)
        self.trays_widget.setObjectName(u"trays_widget")
        self.verticalLayout = QVBoxLayout(self.trays_widget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_2.addWidget(self.trays_widget)

        self.mode_widget = QWidget(self.tool_widget)
        self.mode_widget.setObjectName(u"mode_widget")
        self.verticalLayout_3 = QVBoxLayout(self.mode_widget)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.line_2 = QFrame(self.mode_widget)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.verticalLayout_3.addWidget(self.line_2)

        self.label = QLabel(self.mode_widget)
        self.label.setObjectName(u"label")

        self.verticalLayout_3.addWidget(self.label)

        self.tray_name_widget = QWidget(self.mode_widget)
        self.tray_name_widget.setObjectName(u"tray_name_widget")
        self.horizontalLayout_4 = QHBoxLayout(self.tray_name_widget)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.left_tray_name_lineEdit = QLineEdit(self.tray_name_widget)
        self.left_tray_name_lineEdit.setObjectName(u"left_tray_name_lineEdit")

        self.horizontalLayout_4.addWidget(self.left_tray_name_lineEdit)

        self.right_tray_name_lineEdit = QLineEdit(self.tray_name_widget)
        self.right_tray_name_lineEdit.setObjectName(u"right_tray_name_lineEdit")

        self.horizontalLayout_4.addWidget(self.right_tray_name_lineEdit)


        self.verticalLayout_3.addWidget(self.tray_name_widget)

        self.tray_left_right_horizontalLayout = QHBoxLayout()
        self.tray_left_right_horizontalLayout.setObjectName(u"tray_left_right_horizontalLayout")
        self.tray_left_out_pushButton = QPushButton(self.mode_widget)
        self.tray_left_out_pushButton.setObjectName(u"tray_left_out_pushButton")

        self.tray_left_right_horizontalLayout.addWidget(self.tray_left_out_pushButton)

        self.tray_right_out_pushButton = QPushButton(self.mode_widget)
        self.tray_right_out_pushButton.setObjectName(u"tray_right_out_pushButton")

        self.tray_left_right_horizontalLayout.addWidget(self.tray_right_out_pushButton)


        self.verticalLayout_3.addLayout(self.tray_left_right_horizontalLayout)

        self.line = QFrame(self.mode_widget)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.verticalLayout_3.addWidget(self.line)

        self.label_2 = QLabel(self.mode_widget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setInputMethodHints(Qt.ImhNone)

        self.verticalLayout_3.addWidget(self.label_2)

        self.sample_name_lineEdit = QLineEdit(self.mode_widget)
        self.sample_name_lineEdit.setObjectName(u"sample_name_lineEdit")
        self.sample_name_lineEdit.setEnabled(False)

        self.verticalLayout_3.addWidget(self.sample_name_lineEdit)

        self.pass_pushButton = QPushButton(self.mode_widget)
        self.pass_pushButton.setObjectName(u"pass_pushButton")
        self.pass_pushButton.setEnabled(False)

        self.verticalLayout_3.addWidget(self.pass_pushButton)

        self.line_3 = QFrame(self.mode_widget)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.HLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.verticalLayout_3.addWidget(self.line_3)

        self.label_3 = QLabel(self.mode_widget)
        self.label_3.setObjectName(u"label_3")

        self.verticalLayout_3.addWidget(self.label_3)

        self.mode_comboBox = QComboBox(self.mode_widget)
        self.mode_comboBox.addItem("")
        self.mode_comboBox.addItem("")
        self.mode_comboBox.setObjectName(u"mode_comboBox")

        self.verticalLayout_3.addWidget(self.mode_comboBox)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.reset_goal_pushButton = QPushButton(self.mode_widget)
        self.reset_goal_pushButton.setObjectName(u"reset_goal_pushButton")

        self.horizontalLayout_6.addWidget(self.reset_goal_pushButton)

        self.next_spot_PushButton = QPushButton(self.mode_widget)
        self.next_spot_PushButton.setObjectName(u"next_spot_PushButton")

        self.horizontalLayout_6.addWidget(self.next_spot_PushButton)


        self.verticalLayout_3.addLayout(self.horizontalLayout_6)


        self.verticalLayout_2.addWidget(self.mode_widget)

        self.verticalSpacer = QSpacerItem(20, 10, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(-1, -1, -1, 0)
        self.clean_trays_pushButton = QPushButton(self.tool_widget)
        self.clean_trays_pushButton.setObjectName(u"clean_trays_pushButton")

        self.horizontalLayout_5.addWidget(self.clean_trays_pushButton)

        self.hard_reset_pushButton = QPushButton(self.tool_widget)
        self.hard_reset_pushButton.setObjectName(u"hard_reset_pushButton")

        self.horizontalLayout_5.addWidget(self.hard_reset_pushButton)


        self.verticalLayout_2.addLayout(self.horizontalLayout_5)

        self.exitPushButton = QPushButton(self.tool_widget)
        self.exitPushButton.setObjectName(u"exitPushButton")

        self.verticalLayout_2.addWidget(self.exitPushButton)


        self.horizontalLayout.addWidget(self.tool_widget)

        DryBloodWindow.setCentralWidget(self.centralwidget)
        self.menuBar = QMenuBar(DryBloodWindow)
        self.menuBar.setObjectName(u"menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 1100, 22))
        self.menuFile = QMenu(self.menuBar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuSettings = QMenu(self.menuBar)
        self.menuSettings.setObjectName(u"menuSettings")
        self.menuRun = QMenu(self.menuBar)
        self.menuRun.setObjectName(u"menuRun")
        DryBloodWindow.setMenuBar(self.menuBar)

        self.menuBar.addAction(self.menuFile.menuAction())
        self.menuBar.addAction(self.menuRun.menuAction())
        self.menuBar.addAction(self.menuSettings.menuAction())
        self.menuFile.addAction(self.actionSave)
        self.menuFile.addSeparator()
        self.menuSettings.addAction(self.actionDev_serial)
        self.menuSettings.addAction(self.actionShow_legend_1)
        self.menuSettings.addSeparator()
        self.menuSettings.addAction(self.actionRestore_last_state)
        self.menuSettings.addAction(self.actionService_Mode)
        self.menuSettings.addAction(self.actionSample_presence_test)
        self.menuSettings.addSeparator()
        self.menuSettings.addAction(self.actionRestart)
        self.menuRun.addAction(self.actionModbus_Scan)
        self.menuRun.addAction(self.actionModbus_Connect)
        self.menuRun.addAction(self.actionModbus_Disconnect)

        self.retranslateUi(DryBloodWindow)

        QMetaObject.connectSlotsByName(DryBloodWindow)
    # setupUi

    def retranslateUi(self, DryBloodWindow):
        DryBloodWindow.setWindowTitle(QCoreApplication.translate("DryBloodWindow", u"DBS Controller", None))
        self.actionModbus_Scan.setText(QCoreApplication.translate("DryBloodWindow", u"Scan for Devices", None))
#if QT_CONFIG(shortcut)
        self.actionModbus_Scan.setShortcut(QCoreApplication.translate("DryBloodWindow", u"Ctrl+R", None))
#endif // QT_CONFIG(shortcut)
        self.actionExit.setText(QCoreApplication.translate("DryBloodWindow", u"Exit", None))
#if QT_CONFIG(shortcut)
        self.actionExit.setShortcut(QCoreApplication.translate("DryBloodWindow", u"Ctrl+Q", None))
#endif // QT_CONFIG(shortcut)
        self.actionSettings.setText(QCoreApplication.translate("DryBloodWindow", u"Settings", None))
        self.actionModbus_Connect.setText(QCoreApplication.translate("DryBloodWindow", u"Connect to", None))
        self.actionModbus_Disconnect.setText(QCoreApplication.translate("DryBloodWindow", u"Disconnect", None))
        self.actionSave.setText(QCoreApplication.translate("DryBloodWindow", u"Export to xlsx", None))
#if QT_CONFIG(shortcut)
        self.actionSave.setShortcut(QCoreApplication.translate("DryBloodWindow", u"Ctrl+S", None))
#endif // QT_CONFIG(shortcut)
        self.actionService_Mode.setText(QCoreApplication.translate("DryBloodWindow", u"Service mode", None))
        self.actionDev_serial.setText(QCoreApplication.translate("DryBloodWindow", u"Device serial", None))
        self.actionEmpty.setText(QCoreApplication.translate("DryBloodWindow", u"Empty", None))
        self.actionShow_legend_1.setText(QCoreApplication.translate("DryBloodWindow", u"Show legend", None))
#if QT_CONFIG(shortcut)
        self.actionShow_legend_1.setShortcut(QCoreApplication.translate("DryBloodWindow", u"Ctrl+L", None))
#endif // QT_CONFIG(shortcut)
        self.actionRestart.setText(QCoreApplication.translate("DryBloodWindow", u"Restart device", None))
        self.actionRestore_last_state.setText(QCoreApplication.translate("DryBloodWindow", u"Restore last session", None))
        self.actionSensor_sample_test.setText(QCoreApplication.translate("DryBloodWindow", u"Sensor sample test", None))
        self.actionSample_presence_test.setText(QCoreApplication.translate("DryBloodWindow", u"Sample presence test", None))
        self.user_label.setText(QCoreApplication.translate("DryBloodWindow", u"User:", None))
        self.user_name_lineEdit.setText("")
        self.user_name_lineEdit.setPlaceholderText(QCoreApplication.translate("DryBloodWindow", u"user name", None))
        self.modbus_label.setText(QCoreApplication.translate("DryBloodWindow", u"Connection", None))
        self.dbs_status.setText("")
        self.dbs_label.setText(QCoreApplication.translate("DryBloodWindow", u"DBS status", None))
        self.modbus_status.setText("")
        self.position_label.setText(QCoreApplication.translate("DryBloodWindow", u"Position", None))
        self.position_status.setText("")
        self.label.setText(QCoreApplication.translate("DryBloodWindow", u"Tray:", None))
        self.left_tray_name_lineEdit.setText("")
        self.left_tray_name_lineEdit.setPlaceholderText(QCoreApplication.translate("DryBloodWindow", u"plate name", None))
        self.right_tray_name_lineEdit.setText("")
        self.right_tray_name_lineEdit.setPlaceholderText(QCoreApplication.translate("DryBloodWindow", u"plate right ", None))
        self.tray_left_out_pushButton.setText(QCoreApplication.translate("DryBloodWindow", u"Left Out", None))
        self.tray_right_out_pushButton.setText(QCoreApplication.translate("DryBloodWindow", u"Right Out", None))
#if QT_CONFIG(tooltip)
        self.label_2.setToolTip("")
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        self.label_2.setStatusTip("")
#endif // QT_CONFIG(statustip)
        self.label_2.setText(QCoreApplication.translate("DryBloodWindow", u"Sample name:", None))
        self.pass_pushButton.setText(QCoreApplication.translate("DryBloodWindow", u"Skip Sample", None))
        self.label_3.setText(QCoreApplication.translate("DryBloodWindow", u"Mode:", None))
        self.mode_comboBox.setItemText(0, QCoreApplication.translate("DryBloodWindow", u"Column (A1, B1, C1, ...)", None))
        self.mode_comboBox.setItemText(1, QCoreApplication.translate("DryBloodWindow", u"Row (A1, A2, A3, ...)", None))

        self.reset_goal_pushButton.setText(QCoreApplication.translate("DryBloodWindow", u"Unset Target", None))
        self.next_spot_PushButton.setText(QCoreApplication.translate("DryBloodWindow", u"Next Target", None))
        self.clean_trays_pushButton.setText(QCoreApplication.translate("DryBloodWindow", u"Clean Plate", None))
        self.hard_reset_pushButton.setText(QCoreApplication.translate("DryBloodWindow", u"Reset", None))
        self.exitPushButton.setText(QCoreApplication.translate("DryBloodWindow", u"Exit", None))
        self.menuFile.setTitle(QCoreApplication.translate("DryBloodWindow", u"File", None))
        self.menuSettings.setTitle(QCoreApplication.translate("DryBloodWindow", u"Help", None))
        self.menuRun.setTitle(QCoreApplication.translate("DryBloodWindow", u"Connection", None))
    # retranslateUi

