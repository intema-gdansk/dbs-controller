from PySide2.QtWidgets import QDialog

from gui.uis.legend_dialog import Ui_Dialog


class DBSLegendDialog(QDialog):
    def __init__(self, parent=None):
        super(DBSLegendDialog, self).__init__(parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.setWindowTitle("Legend")
