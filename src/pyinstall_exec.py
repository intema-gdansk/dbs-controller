import PyInstaller.__main__
import os

args = [
	'main_gui.py',
	'--onefile',
	'--name', 'DBScontroller',
]

if os.name == 'nt':
	args.extend(['--windowed', '--icon', 'gui\\uis\\icons\\Red_blood_cell.ico'])
	args.extend(['--add-data', 'config.json;.'])
else:
	args.extend(['--add-data', 'config.json:.'])
PyInstaller.__main__.run(args)
