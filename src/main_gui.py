import sys
from PySide2.QtWidgets import QApplication
from gui.dbs_window import DryBloodSystem


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle("fusion")
    mainWindow = DryBloodSystem()
    mainWindow.show()
    sys.exit(app.exec_())
