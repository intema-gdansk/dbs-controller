# DBS controller

Application for control Dry Blood Spot Puncher (DBS) machine created by Intema Sp. z o.o. The last version for Windows for one plate DBS is located in [webpage](https://gitlab.com/intema-gdansk/dbs-controller/-/blob/main/dst/v1.4.1/DBScontroller_1.4.1S.exe). If the application does not recognize your DBS, you should install USB [driver](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers). It is also recommended to use application in Full HD resolution without scaling. 

You may order the machine at [Intema webpage](http://www.intema.pl). Our machines are commonly used in hospitals with cutting machines to analyze a dry blood drop. DBS has a movable working table with two 96-well plates, which is automatically controlled by our software. The operator, pressing the table, cuts out a tissue disc falling into the selected well. The design of the device ensures convenient and quick sample preparation for diagnostic test.

# DBS machine
![](/imags/dbs.jpg)

# Changelog
## [v1.5.1] - 2023-02-13 (requires ITM552 or newer version of instrument)
- Changed the required plate parameters in the settings window
- Added limit check when provaiding new plate parameters
- Minor bug fixes

## [v1.5.0] - 2023-12-22 (requires ITM552 or newer version of instrument)
- Added selection to work on different plates
- Added reading of instrument accessories when connecting 
- Added option to block multiple spots in settings
- Added static elimination button on the toolbar
- Added option to exchange knife
- Minor improvements and bug fixes

## [v1.4.1] - 2023-05-16
- Changes in excel worksheet export (two new sheets added)
- Minor fixes in session restoring
- 1.4.1S - version of application for DBS machines that work only with single plate

## [v1.4.0] - 2022-09-30 (works only with two plates DBS machines)
- Added number of cuts selector
- Added support for new possible machine errors

## [v1.3.0] - 2022-05-10 (works only with two plates DBS machines)
- Added support for working with two plates
- Changed "Service mode" into "Standard name"
- Added option to enable/disable punch cleaning
- Small changes in "Tools panel" in GUI
- Support for handling new errors from limit switches

## [v1.2.1] - 2022-05-04
- Fixed a bug while double-clicking at processed spot

## [v1.2.0] - 2021-07-29
- Added option to disable/enable sample presence test
- Minor bug fixes

## [v1.1.0] - 2021-06-18
- Added functionality of restoring a closed session state
- Added a DBS state logger
- Updated the debug logger
- Fixed a bug with interface freezing
- Fixed a bug that caused missed machine state changes

## [v1.0.2] - 2021-06-09
- Added a debug logger
- Fixed a bug causing the application to slow down after sending Tray Out CMD to a disconnected device

## [v1.0.1] - 2021-04-28
- Added confirmation dialogs before cleaning plates and closing the application

## [v1.0.0] - 2021-04-23
- First release

